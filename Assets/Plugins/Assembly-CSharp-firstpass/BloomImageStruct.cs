using UnityEngine;

public struct BloomImageStruct
{
	public Color color;

	public float radius;
}
