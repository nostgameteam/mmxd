Shader "Cartoon FX/Alpha Blended + Additive" {
	Properties {
		_TintColor ("Tint Color", Vector) = (0.5,0.5,0.5,0.5)
		_MainTex ("Particle Texture", 2D) = "white" {}
		_InvFade ("Soft Particles Factor", Range(0.01, 3)) = 1
	}
	SubShader {
		Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		UsePass "Hidden/Cartoon FX/Particles/ALPHA_BLENDED_VCOLOR"
		UsePass "Hidden/Cartoon FX/Particles/ADDITIVE"
	}
}