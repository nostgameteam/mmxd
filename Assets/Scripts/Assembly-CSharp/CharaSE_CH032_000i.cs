public static class CharaSE_CH032_000i
{
	public const int iCRI_CHARASE_CH032_000_CUENUM = 10;

	public const int iCRI_CHARASE_CH032_000_CH032_JUMP = 1;

	public const int iCRI_CHARASE_CH032_000_CH032_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_CH032_000_CH032_CHAKUCHI = 3;

	public const int iCRI_CHARASE_CH032_000_CH032_DASH = 4;

	public const int iCRI_CHARASE_CH032_000_CH032_DASHEND = 5;

	public const int iCRI_CHARASE_CH032_000_CH032_KABEPETA = 6;

	public const int iCRI_CHARASE_CH032_000_CH032_KABEKERI = 7;

	public const int iCRI_CHARASE_CH032_000_CH032_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_CH032_000_CH032_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_CH032_000_CH032_FOOT = 10;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CUENUM = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CUENUM;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_JUMP = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_JUMP;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_JUMP_HIGH = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_JUMP_HIGH;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_CHAKUCHI = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_CHAKUCHI;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_DASH = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_DASH;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_DASHEND = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_DASHEND;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_KABEPETA = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_KABEPETA;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_KABEKERI = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_KABEKERI;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_TELEPORT_IN = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_TELEPORT_IN;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_TELEPORT_OUT = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CH032_TELEPORT_OUT;

	public const CharaSE_CH032_000 CRI_CHARASE_CH032_000_CH032_FOOT = CharaSE_CH032_000.CRI_CHARASE_CH032_000_CUENUM;
}
