public enum SkillSE_BLUES
{
	NONE = 0,
	CRI_SKILLSE_BLUES_CUENUM = 10,
	CRI_SKILLSE_BLUES_BL_CHARGE_LP = 1,
	CRI_SKILLSE_BLUES_BL_CHARGE_STOP = 2,
	CRI_SKILLSE_BLUES_BL_CHARGESHOT_L = 5,
	CRI_SKILLSE_BLUES_BL_SHIELD01 = 7,
	CRI_SKILLSE_BLUES_BL_SHIELD02 = 8,
	CRI_SKILLSE_BLUES_BL_SHIELD03 = 9,
	CRI_SKILLSE_BLUES_BL_CHARGESHOT_M = 4,
	CRI_SKILLSE_BLUES_BL_CHARGESHOT_S = 3,
	CRI_SKILLSE_BLUES_BL_CHARGEMAX = 6,
	CRI_SKILLSE_BLUES_BL_KUCHIBUE = 11
}
