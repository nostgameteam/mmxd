public static class VOICE_VIAi
{
	public const int iCRI_VOICE_VIA_CUENUM = 21;

	public const int iCRI_VOICE_VIA_V_VI_HURT01 = 1;

	public const int iCRI_VOICE_VIA_V_VI_START01 = 2;

	public const int iCRI_VOICE_VIA_V_VI_WIN01 = 3;

	public const int iCRI_VOICE_VIA_V_VI_LOSE01 = 5;

	public const int iCRI_VOICE_VIA_V_VI_UNIQUE01 = 6;

	public const int iCRI_VOICE_VIA_V_VI_UNIQUE02 = 7;

	public const int iCRI_VOICE_VIA_V_VI_SKILL01 = 8;

	public const int iCRI_VOICE_VIA_V_VI_SKILL02 = 9;

	public const int iCRI_VOICE_VIA_V_VI_SKILL03 = 10;

	public const int iCRI_VOICE_VIA_V_VI_SKILL04 = 11;

	public const int iCRI_VOICE_VIA_V_VI_JUMP01 = 12;

	public const int iCRI_VOICE_VIA_V_VI_SABER01 = 14;

	public const int iCRI_VOICE_VIA_V_VI_SABER03 = 16;

	public const int iCRI_VOICE_VIA_V_VI_SABER05 = 18;

	public const int iCRI_VOICE_VIA_V_VI_PINCH01 = 20;

	public const int iCRI_VOICE_VIA_V_VI_PINCH02 = 21;

	public const int iCRI_VOICE_VIA_V_VI_PINCH03 = 22;

	public const int iCRI_VOICE_VIA_V_VI_ATTACK01 = 25;

	public const int iCRI_VOICE_VIA_V_VI_CHARA01 = 40;

	public const int iCRI_VOICE_VIA_V_VI_CHARA02 = 42;

	public const int iCRI_VOICE_VIA_V_VI_WIN02 = 4;

	public const VOICE_VIA CRI_VOICE_VIA_CUENUM = VOICE_VIA.CRI_VOICE_VIA_CUENUM;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_HURT01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_HURT01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_START01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_START01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_WIN01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_WIN01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_LOSE01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_LOSE01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_UNIQUE01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_UNIQUE01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_UNIQUE02 = VOICE_VIA.CRI_VOICE_VIA_V_VI_UNIQUE02;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SKILL01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SKILL01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SKILL02 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SKILL02;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SKILL03 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SKILL03;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SKILL04 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SKILL04;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_JUMP01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_JUMP01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SABER01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SABER01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SABER03 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SABER03;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_SABER05 = VOICE_VIA.CRI_VOICE_VIA_V_VI_SABER05;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_PINCH01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_PINCH01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_PINCH02 = VOICE_VIA.CRI_VOICE_VIA_CUENUM;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_PINCH03 = VOICE_VIA.CRI_VOICE_VIA_V_VI_PINCH03;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_ATTACK01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_ATTACK01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_CHARA01 = VOICE_VIA.CRI_VOICE_VIA_V_VI_CHARA01;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_CHARA02 = VOICE_VIA.CRI_VOICE_VIA_V_VI_CHARA02;

	public const VOICE_VIA CRI_VOICE_VIA_V_VI_WIN02 = VOICE_VIA.CRI_VOICE_VIA_V_VI_WIN02;
}
