using UnityEngine;
using UnityEngine.UI;

public class CrusadeTimeInfo : MonoBehaviour
{
	public Transform PlayerIconRoot;

	public Text TimeInfoText;

	public Text AttackTimeText;
}
