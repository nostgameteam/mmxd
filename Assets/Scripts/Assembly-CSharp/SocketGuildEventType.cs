public enum SocketGuildEventType
{
	MemberJoined = 0,
	MemberKicked = 1,
	MemberPrivilegeChanged = 2,
	HeaderPowerChanged = 3,
	GuildRemoved = 4,
	GuildRankup = 5,
	PowerTowerRankup = 6,
	PowerPillarChanged = 7,
	OreChanged = 8
}
