public interface IF_ForceExecute
{
	bool SetMission(int mission = -1);

	bool ForceExecuteMission();

	bool SetStopMission();
}
