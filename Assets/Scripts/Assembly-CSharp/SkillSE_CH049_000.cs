public enum SkillSE_CH049_000
{
	NONE = 0,
	CRI_SKILLSE_CH049_000_CUENUM = 9,
	CRI_SKILLSE_CH049_000_CH049_SHOT_01 = 1,
	CRI_SKILLSE_CH049_000_CH049_SHOT_02 = 2,
	CRI_SKILLSE_CH049_000_CH049_SHOT_03 = 3,
	CRI_SKILLSE_CH049_000_CH049_CHARGE_LP = 4,
	CRI_SKILLSE_CH049_000_CH049_CHARGE_STOP = 5,
	CRI_SKILLSE_CH049_000_CH049_STRAIGHT01 = 6,
	CRI_SKILLSE_CH049_000_CH049_STRAIGHT02 = 7,
	CRI_SKILLSE_CH049_000_CH049_CHARA01 = 8,
	CRI_SKILLSE_CH049_000_CH049_START01 = 9
}
