using System;
using System.Collections.Generic;

[Serializable]
public class FacebookQueryData
{
	public List<FacebookQueryUser> data = new List<FacebookQueryUser>();
}
