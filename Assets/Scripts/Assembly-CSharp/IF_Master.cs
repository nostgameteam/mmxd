public interface IF_Master
{
	void ReportObjects(object[] values);

	object[] GetValues(object[] param = null);
}
