using System;
using UnityEngine.Events;

namespace MagicaCloth
{
	[Serializable]
	public class AvatarPartsDetachEvent : UnityEvent<MagicaAvatar>
	{
	}
}
