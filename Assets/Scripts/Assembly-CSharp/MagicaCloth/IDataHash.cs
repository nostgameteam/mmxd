namespace MagicaCloth
{
	public interface IDataHash
	{
		int GetDataHash();
	}
}
