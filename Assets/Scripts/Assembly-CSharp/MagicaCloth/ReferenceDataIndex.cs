using System;

namespace MagicaCloth
{
	[Serializable]
	public struct ReferenceDataIndex
	{
		public int startIndex;

		public int count;
	}
}
