public enum VOICE_XMH
{
	NONE = 0,
	CRI_VOICE_XMH_CUENUM = 18,
	CRI_VOICE_XMH_V_XM_HURT01 = 1,
	CRI_VOICE_XMH_V_XM_ATTACK01 = 25,
	CRI_VOICE_XMH_V_XM_SKILL01 = 8,
	CRI_VOICE_XMH_V_XM_PINCH01 = 20,
	CRI_VOICE_XMH_V_XM_PINCH02 = 21,
	CRI_VOICE_XMH_V_XM_PINCH03 = 22,
	CRI_VOICE_XMH_V_XM_LOSE01 = 5,
	CRI_VOICE_XMH_V_XM_UNIQUE01 = 6,
	CRI_VOICE_XMH_V_XM_WIN01 = 3,
	CRI_VOICE_XMH_V_XM_JUMP01 = 12,
	CRI_VOICE_XMH_V_XM_SABER01 = 14,
	CRI_VOICE_XMH_V_XM_SABER02 = 15,
	CRI_VOICE_XMH_V_XM_SABER03 = 16,
	CRI_VOICE_XMH_V_XM_SABER04 = 17,
	CRI_VOICE_XMH_V_XM_SABER05 = 18,
	CRI_VOICE_XMH_V_XM_START01 = 2,
	CRI_VOICE_XMH_V_XM_SKILL02_2 = 10,
	CRI_VOICE_XMH_V_XM_SKILL02_1 = 9
}
