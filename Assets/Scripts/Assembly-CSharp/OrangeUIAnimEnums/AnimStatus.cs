namespace OrangeUIAnimEnums
{
	public enum AnimStatus
	{
		LOADING = 0,
		SHOWING = 1,
		COMPLETE = 2
	}
}
