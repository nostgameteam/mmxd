public enum AnalogSticks
{
	MAIN = 0,
	SUB0 = 1,
	SUB1 = 2,
	SUB2 = 3,
	SUB3 = 4,
	SUB4 = 5,
	MAX_ANALOGSTICKS = 6
}
