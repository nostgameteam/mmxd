public enum SkillSE_TRON2
{
	NONE = 0,
	CRI_SKILLSE_TRON2_CUENUM = 12,
	CRI_SKILLSE_TRON2_TR2_B01 = 1,
	CRI_SKILLSE_TRON2_TR2_B02 = 2,
	CRI_SKILLSE_TRON2_TR2_B03 = 3,
	CRI_SKILLSE_TRON2_TR2_Y01_LP = 5,
	CRI_SKILLSE_TRON2_TR2_Y01_STOP = 6,
	CRI_SKILLSE_TRON2_TR2_KBMOLE02_STOP = 9,
	CRI_SKILLSE_TRON2_TR2_CHARA01 = 40,
	CRI_SKILLSE_TRON2_TR2_CHARA02 = 41,
	CRI_SKILLSE_TRON2_TR2_KBMOLE02_LP = 8,
	CRI_SKILLSE_TRON2_TR2_KBMOLE01 = 7,
	CRI_SKILLSE_TRON2_TR2_CANNON01 = 11,
	CRI_SKILLSE_TRON2_TR2_CANNON02 = 12
}
