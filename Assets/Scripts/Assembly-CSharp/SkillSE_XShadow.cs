public enum SkillSE_XShadow
{
	NONE = 0,
	CRI_SKILLSE_XSHADOW_CUENUM = 8,
	CRI_SKILLSE_XSHADOW_XS_GIGA01 = 1,
	CRI_SKILLSE_XSHADOW_XS_GIGA02_LP = 2,
	CRI_SKILLSE_XSHADOW_XS_RING_STOP = 8,
	CRI_SKILLSE_XSHADOW_XS_RING_LP = 7,
	CRI_SKILLSE_XSHADOW_XS_GIGA02_STOP = 3,
	CRI_SKILLSE_XSHADOW_XS_SHOT = 5,
	CRI_SKILLSE_XSHADOW_XS_CHARA01 = 40,
	CRI_SKILLSE_XSHADOW_XS_START01 = 80
}
