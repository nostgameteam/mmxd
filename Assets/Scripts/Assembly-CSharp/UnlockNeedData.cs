public class UnlockNeedData
{
	public ITEM_TABLE item;

	public int amount;

	public UnlockNeedData(ITEM_TABLE item, int amount)
	{
		this.item = item;
		this.amount = amount;
	}
}
