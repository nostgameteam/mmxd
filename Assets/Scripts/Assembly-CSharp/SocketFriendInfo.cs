using enums;

public class SocketFriendInfo
{
	public string FriendPlayerID = string.Empty;

	public UserStatus Status;

	public int Busy;

	public int Follow;

	public string FriendPlayerHUD = string.Empty;
}
