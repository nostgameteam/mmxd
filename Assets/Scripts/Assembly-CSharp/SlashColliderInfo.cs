using UnityEngine;

public class SlashColliderInfo
{
	public Vector2 offset;

	public Vector2 size;

	public float timing;
}
