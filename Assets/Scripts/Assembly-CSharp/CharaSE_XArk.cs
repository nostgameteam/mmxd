public enum CharaSE_XArk
{
	NONE = 0,
	CRI_CHARASE_XARK_CUENUM = 10,
	CRI_CHARASE_XARK_XA_JUMP = 1,
	CRI_CHARASE_XARK_XA_JUMP_HIGH = 2,
	CRI_CHARASE_XARK_XA_CHAKUCHI = 3,
	CRI_CHARASE_XARK_XA_DASH = 4,
	CRI_CHARASE_XARK_XA_DASHEND = 5,
	CRI_CHARASE_XARK_XA_KABEPETA = 6,
	CRI_CHARASE_XARK_XA_KABEKERI = 7,
	CRI_CHARASE_XARK_XA_TELEPORT_IN = 8,
	CRI_CHARASE_XARK_XA_TELEPORT_OUT = 9,
	CRI_CHARASE_XARK_XA_FOOT = 10
}
