public static class HitSE02i
{
	public const int iCRI_HITSE02_CUENUM = 2;

	public const int iCRI_HITSE02_HT_WATER = 108;

	public const int iCRI_HITSE02_HT_VIRUS = 109;

	public const HitSE02 CRI_HITSE02_CUENUM = HitSE02.CRI_HITSE02_CUENUM;

	public const HitSE02 CRI_HITSE02_HT_WATER = HitSE02.CRI_HITSE02_HT_WATER;

	public const HitSE02 CRI_HITSE02_HT_VIRUS = HitSE02.CRI_HITSE02_HT_VIRUS;
}
