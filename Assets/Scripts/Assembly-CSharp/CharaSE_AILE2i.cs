public static class CharaSE_AILE2i
{
	public const int iCRI_CHARASE_AILE2_CUENUM = 10;

	public const int iCRI_CHARASE_AILE2_AI2_JUMP = 1;

	public const int iCRI_CHARASE_AILE2_AI2_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_AILE2_AI2_CHAKUCHI = 3;

	public const int iCRI_CHARASE_AILE2_AI2_DASH = 4;

	public const int iCRI_CHARASE_AILE2_AI2_DASHEND = 5;

	public const int iCRI_CHARASE_AILE2_AI2_KABEPETA = 6;

	public const int iCRI_CHARASE_AILE2_AI2_KABEKERI = 7;

	public const int iCRI_CHARASE_AILE2_AI2_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_AILE2_AI2_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_AILE2_AI2_FOOT = 10;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_CUENUM = CharaSE_AILE2.CRI_CHARASE_AILE2_CUENUM;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_JUMP = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_JUMP;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_JUMP_HIGH = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_JUMP_HIGH;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_CHAKUCHI = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_CHAKUCHI;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_DASH = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_DASH;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_DASHEND = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_DASHEND;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_KABEPETA = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_KABEPETA;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_KABEKERI = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_KABEKERI;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_TELEPORT_IN = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_TELEPORT_IN;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_TELEPORT_OUT = CharaSE_AILE2.CRI_CHARASE_AILE2_AI2_TELEPORT_OUT;

	public const CharaSE_AILE2 CRI_CHARASE_AILE2_AI2_FOOT = CharaSE_AILE2.CRI_CHARASE_AILE2_CUENUM;
}
