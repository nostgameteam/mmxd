public enum STORY_IRIS
{
	NONE = 0,
	CRI_STORY_IRIS_CUENUM = 27,
	CRI_STORY_IRIS_S_IR_STORY01 = 1,
	CRI_STORY_IRIS_S_IR_STORY02 = 2,
	CRI_STORY_IRIS_S_IR_STORY03 = 3,
	CRI_STORY_IRIS_S_IR_STORY04 = 4,
	CRI_STORY_IRIS_S_IR_STORY05 = 5,
	CRI_STORY_IRIS_S_IR_STORY06 = 6,
	CRI_STORY_IRIS_S_IR_STORY07 = 7,
	CRI_STORY_IRIS_S_IR_STORY08 = 8,
	CRI_STORY_IRIS_S_IR_STORY09 = 9,
	CRI_STORY_IRIS_S_IR_STORY10 = 10,
	CRI_STORY_IRIS_S_IR_STORY11 = 11,
	CRI_STORY_IRIS_S_IR_STORY12 = 12,
	CRI_STORY_IRIS_S_IR_STORY13 = 13,
	CRI_STORY_IRIS_S_IR_STORY14 = 14,
	CRI_STORY_IRIS_S_IR_STORY15 = 15,
	CRI_STORY_IRIS_S_IR_STORY16 = 16,
	CRI_STORY_IRIS_S_IR_STORY17 = 17,
	CRI_STORY_IRIS_S_IR_STORY18 = 18,
	CRI_STORY_IRIS_S_IR_STORY19 = 19,
	CRI_STORY_IRIS_S_IR_STORY20 = 20,
	CRI_STORY_IRIS_S_IR_STORY21 = 21,
	CRI_STORY_IRIS_S_IR_STORY22 = 22,
	CRI_STORY_IRIS_S_IR_STORY23 = 23,
	CRI_STORY_IRIS_S_IR_STORY24 = 24,
	CRI_STORY_IRIS_S_IR_STORY25 = 25,
	CRI_STORY_IRIS_S_IR_STORY26 = 26,
	CRI_STORY_IRIS_S_IR_STORY27 = 27
}
