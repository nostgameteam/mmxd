public static class VOICE_RICOi
{
	public const int iCRI_VOICE_RICO_CUENUM = 23;

	public const int iCRI_VOICE_RICO_V_RI_HURT01 = 1;

	public const int iCRI_VOICE_RICO_V_RI_ATTACK01 = 25;

	public const int iCRI_VOICE_RICO_V_RI_SKILL01 = 8;

	public const int iCRI_VOICE_RICO_V_RI_SKILL04 = 11;

	public const int iCRI_VOICE_RICO_V_RI_SKILL03 = 10;

	public const int iCRI_VOICE_RICO_V_RI_SKILL02 = 9;

	public const int iCRI_VOICE_RICO_V_RI_PINCH01 = 20;

	public const int iCRI_VOICE_RICO_V_RI_PINCH02 = 21;

	public const int iCRI_VOICE_RICO_V_RI_PINCH03 = 22;

	public const int iCRI_VOICE_RICO_V_RI_LOSE01 = 5;

	public const int iCRI_VOICE_RICO_V_RI_UNIQUE02 = 7;

	public const int iCRI_VOICE_RICO_V_RI_CHARA01 = 40;

	public const int iCRI_VOICE_RICO_V_RI_CHARA02 = 41;

	public const int iCRI_VOICE_RICO_V_RI_CHARA03 = 42;

	public const int iCRI_VOICE_RICO_V_RI_WIN01 = 3;

	public const int iCRI_VOICE_RICO_V_RI_JUMP01 = 12;

	public const int iCRI_VOICE_RICO_V_RI_SABER01 = 14;

	public const int iCRI_VOICE_RICO_V_RI_SABER02 = 15;

	public const int iCRI_VOICE_RICO_V_RI_SABER03 = 16;

	public const int iCRI_VOICE_RICO_V_RI_SABER04 = 17;

	public const int iCRI_VOICE_RICO_V_RI_SABER05 = 18;

	public const int iCRI_VOICE_RICO_V_RI_START01 = 2;

	public const int iCRI_VOICE_RICO_V_RI_UNIQUE01 = 6;

	public const VOICE_RICO CRI_VOICE_RICO_CUENUM = VOICE_RICO.CRI_VOICE_RICO_CUENUM;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_HURT01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_HURT01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_ATTACK01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_ATTACK01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SKILL01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SKILL01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SKILL04 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SKILL04;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SKILL03 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SKILL03;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SKILL02 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SKILL02;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_PINCH01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_PINCH01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_PINCH02 = VOICE_RICO.CRI_VOICE_RICO_V_RI_PINCH02;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_PINCH03 = VOICE_RICO.CRI_VOICE_RICO_V_RI_PINCH03;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_LOSE01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_LOSE01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_UNIQUE02 = VOICE_RICO.CRI_VOICE_RICO_V_RI_UNIQUE02;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_CHARA01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_CHARA01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_CHARA02 = VOICE_RICO.CRI_VOICE_RICO_V_RI_CHARA02;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_CHARA03 = VOICE_RICO.CRI_VOICE_RICO_V_RI_CHARA03;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_WIN01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_WIN01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_JUMP01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_JUMP01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SABER01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SABER01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SABER02 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SABER02;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SABER03 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SABER03;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SABER04 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SABER04;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_SABER05 = VOICE_RICO.CRI_VOICE_RICO_V_RI_SABER05;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_START01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_START01;

	public const VOICE_RICO CRI_VOICE_RICO_V_RI_UNIQUE01 = VOICE_RICO.CRI_VOICE_RICO_V_RI_UNIQUE01;
}
