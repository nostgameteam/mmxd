public enum ButtonId
{
	NONE = 0,
	UP = 1,
	DOWN = 2,
	LEFT = 3,
	RIGHT = 4,
	SHOOT = 5,
	JUMP = 6,
	DASH = 7,
	SKILL0 = 8,
	SKILL1 = 9,
	SELECT = 10,
	FS_SKILL = 11,
	CHIP_SWITCH = 12,
	AIM_UP = 13,
	AIM_DOWN = 14,
	AIM_LEFT = 15,
	AIM_RIGHT = 16,
	START = 17,
	OPTION = 18,
	MAX_BUTTON_ID = 19
}
