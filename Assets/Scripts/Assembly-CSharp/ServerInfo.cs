public class ServerInfo
{
	public sbyte DailyResetTime;

	public sbyte TimeZone;

	public long ClientDifferTime;

	public NetResetTimeInfo DailyResetInfo;

	public NetResetTimeInfo WeeklyResetInfo;

	public NetResetTimeInfo MonthlyResetInfo;

	public uint DataCRC;

	public uint ExDataCRC;
}
