public enum CharaSE_RMEXE
{
	NONE = 0,
	CRI_CHARASE_RMEXE_CUENUM = 10,
	CRI_CHARASE_RMEXE_RE_JUMP = 1,
	CRI_CHARASE_RMEXE_RE_JUMP_HIGH = 2,
	CRI_CHARASE_RMEXE_RE_CHAKUCHI = 3,
	CRI_CHARASE_RMEXE_RE_DASH = 4,
	CRI_CHARASE_RMEXE_RE_DASHEND = 5,
	CRI_CHARASE_RMEXE_RE_KABEPETA = 6,
	CRI_CHARASE_RMEXE_RE_KABEKERI = 7,
	CRI_CHARASE_RMEXE_RE_TELEPORT_IN = 8,
	CRI_CHARASE_RMEXE_RE_TELEPORT_OUT = 9,
	CRI_CHARASE_RMEXE_RE_FOOT = 10
}
