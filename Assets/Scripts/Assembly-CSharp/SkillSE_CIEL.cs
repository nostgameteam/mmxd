public enum SkillSE_CIEL
{
	NONE = 0,
	CRI_SKILLSE_CIEL_CUENUM = 22,
	CRI_SKILLSE_CIEL_CL_ELF01_LP = 1,
	CRI_SKILLSE_CIEL_CL_ELFT03 = 15,
	CRI_SKILLSE_CIEL_CL_ELFT02_STOP = 14,
	CRI_SKILLSE_CIEL_CL_ELFT02_LP = 13,
	CRI_SKILLSE_CIEL_CL_ELFT01 = 12,
	CRI_SKILLSE_CIEL_CL_ELFX03 = 20,
	CRI_SKILLSE_CIEL_CL_ELFX02_STOP = 19,
	CRI_SKILLSE_CIEL_CL_ELFX02_LP = 18,
	CRI_SKILLSE_CIEL_CL_ELFX01 = 17,
	CRI_SKILLSE_CIEL_CL_ELF01_STOP = 2,
	CRI_SKILLSE_CIEL_CL_ELF02 = 3,
	CRI_SKILLSE_CIEL_CL_SYSTEM01 = 4,
	CRI_SKILLSE_CIEL_CL_SYSTEM02_LP = 5,
	CRI_SKILLSE_CIEL_CL_SYSTEM02_STOP = 6,
	CRI_SKILLSE_CIEL_CL_CHARA01 = 8,
	CRI_SKILLSE_CIEL_CL_CHARA02 = 9,
	CRI_SKILLSE_CIEL_CL_CHARA03 = 10,
	CRI_SKILLSE_CIEL_CL_CHARA04 = 11,
	CRI_SKILLSE_CIEL_CL_CHOCO01 = 7,
	CRI_SKILLSE_CIEL_CL_CHARA05 = 40,
	CRI_SKILLSE_CIEL_CL_CHARA06 = 41,
	CRI_SKILLSE_CIEL_CL_START06 = 80
}
