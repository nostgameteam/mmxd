public enum WantedTargetState
{
	Normal = 0,
	Started = 1,
	Finished = 2,
	Received = 3
}
