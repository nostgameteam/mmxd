public enum BGM02
{
	NONE = 0,
	CRI_BGM02_CUENUM = 42,
	CRI_BGM02_BGM_SYS_EVENT03 = 1,
	CRI_BGM02_BGM_ST_CRYSTAL = 3,
	CRI_BGM02_BGM_ST_ELECTRIC = 2,
	CRI_BGM02_BGM_ST_VOLCANO = 4,
	CRI_BGM02_BGM_ST_VIO = 5,
	CRI_BGM02_BGM_ST_SNOW = 6,
	CRI_BGM02_BGM_ST_REPLI_AIRFORCE = 7,
	CRI_BGM02_BGM_ST_DENSAN = 9,
	CRI_BGM02_BGM_ST_BOSS08 = 10,
	CRI_BGM02_BGM_SYS_GUILD = 11,
	CRI_BGM02_BGM_SYS_MAIN02 = 12,
	CRI_BGM02_BGM_ST_BOSS09 = 13,
	CRI_BGM02_BGM_ST_OLDCASTLE = 14,
	CRI_BGM02_BGM_ST_BOSS10 = 16,
	CRI_BGM02_BGM_ST_ELECTRO_FORT = 8,
	CRI_BGM02_BGM_SYS_DNA = 17,
	CRI_BGM02_BGM_ST_BOSS11 = 18,
	CRI_BGM02_BGM_ST_BOSS12 = 19,
	CRI_BGM02_BGM_SYS_EVENT04 = 20,
	CRI_BGM02_BGM_ST_NULLSPACE = 21,
	CRI_BGM02_BGM_ST_GRENADE = 22,
	CRI_BGM02_BGM_ST_BOSS13 = 23,
	CRI_BGM02_BGM_SYS_TOTALWAR = 24,
	CRI_BGM02_BGM_ST_PVP = 25,
	CRI_BGM02_BGM_ST_RANK = 26,
	CRI_BGM02_BGM_ST_POWERPLANT = 27,
	CRI_BGM02_BGM_ST_BOSS14 = 28,
	CRI_BGM02_BGM_ST_BOSS15 = 29,
	CRI_BGM02_BGM_ST_BOSS16 = 30,
	CRI_BGM02_BGM_ST_BOSS17 = 31,
	CRI_BGM02_BGM_ST_BOSS18 = 32,
	CRI_BGM02_BGM_ST_BOSS18_2 = 42,
	CRI_BGM02_BGM_ST_BOSS18_3 = 43,
	CRI_BGM02_BGM_ST_SELECT = 33,
	CRI_BGM02_BGM_ST_BOSS19 = 34,
	CRI_BGM02_BGM_ST_BOSS20 = 35,
	CRI_BGM02_BGM_ST_BOSS21 = 36,
	CRI_BGM02_BGM_SYS_STORY = 37,
	CRI_BGM02_BGM_SYS_TITLE02 = 38,
	CRI_BGM02_BGM_ST_BOSS22 = 39,
	CRI_BGM02_BGM_ST_BOSS23 = 40,
	CRI_BGM02_BGM_JIN_CLEAR05 = 41
}
