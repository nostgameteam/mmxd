public enum VOICE_XDive
{
	NONE = 0,
	CRI_VOICE_XDIVE_CUENUM = 17,
	CRI_VOICE_XDIVE_V_XD_HURT01 = 1,
	CRI_VOICE_XDIVE_V_XD_ATTACK01 = 25,
	CRI_VOICE_XDIVE_V_XD_SKILL01 = 8,
	CRI_VOICE_XDIVE_V_XD_SKILL02 = 9,
	CRI_VOICE_XDIVE_V_XD_PINCH01 = 20,
	CRI_VOICE_XDIVE_V_XD_PINCH02 = 21,
	CRI_VOICE_XDIVE_V_XD_PINCH03 = 22,
	CRI_VOICE_XDIVE_V_XD_LOSE01 = 5,
	CRI_VOICE_XDIVE_V_XD_UNIQUE01 = 6,
	CRI_VOICE_XDIVE_V_XD_WIN01 = 3,
	CRI_VOICE_XDIVE_V_XD_JUMP01 = 12,
	CRI_VOICE_XDIVE_V_XD_SABER01 = 14,
	CRI_VOICE_XDIVE_V_XD_SABER02 = 15,
	CRI_VOICE_XDIVE_V_XD_SABER03 = 16,
	CRI_VOICE_XDIVE_V_XD_SABER04 = 17,
	CRI_VOICE_XDIVE_V_XD_SABER05 = 18,
	CRI_VOICE_XDIVE_V_XD_START01 = 2
}
