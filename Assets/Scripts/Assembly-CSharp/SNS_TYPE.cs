public enum SNS_TYPE
{
	NONE = 0,
	GUEST = 1,
	INHERIT = 2,
	FACEBOOK = 3,
	LINE = 4,
	TWITTER = 5,
	APPLE = 6,
	STEAM = 7
}
