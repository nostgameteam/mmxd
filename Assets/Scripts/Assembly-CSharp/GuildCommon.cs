public static class GuildCommon
{
	public const float GUILD_BADGE_COLOR_DIV_BASE = 360f;

	public const int GUILD_LIST_PAGE_COUNT = 20;

	public const string PLAYER_NAME_UNDEFINED = "---";

	public const string GUILD_NAME_UNDEFINED = "---";

	public const int GUILD_EDDIE_DONATE_STEP_COUNT = 4;
}
