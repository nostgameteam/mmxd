public static class SkillSE_CH043_000i
{
	public const int iCRI_SKILLSE_CH043_000_CUENUM = 10;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_LP = 1;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_STOP = 2;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT01 = 3;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT02 = 4;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_MAX01 = 5;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARGE_MAX02 = 6;

	public const int iCRI_SKILLSE_CH043_000_CH043_ARMOR = 7;

	public const int iCRI_SKILLSE_CH043_000_CH043_AIRDASH = 8;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARA01 = 9;

	public const int iCRI_SKILLSE_CH043_000_CH043_CHARA02 = 10;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CUENUM = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CUENUM;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_LP = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_LP;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_STOP = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_STOP;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT01 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT01;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT02 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_SHOT02;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_MAX01 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_MAX01;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARGE_MAX02 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARGE_MAX02;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_ARMOR = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_ARMOR;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_AIRDASH = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_AIRDASH;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARA01 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CH043_CHARA01;

	public const SkillSE_CH043_000 CRI_SKILLSE_CH043_000_CH043_CHARA02 = SkillSE_CH043_000.CRI_SKILLSE_CH043_000_CUENUM;
}
