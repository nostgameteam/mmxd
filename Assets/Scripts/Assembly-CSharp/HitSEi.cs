public static class HitSEi
{
	public const int iCRI_HITSE_CUENUM = 37;

	public const int iCRI_HITSE_HT_SHOT02 = 2;

	public const int iCRI_HITSE_HT_SHOT01 = 1;

	public const int iCRI_HITSE_HT_SHOT03 = 3;

	public const int iCRI_HITSE_HT_CUT01 = 4;

	public const int iCRI_HITSE_HT_CUT02 = 5;

	public const int iCRI_HITSE_HT_CUT03 = 6;

	public const int iCRI_HITSE_HT_BOM01 = 7;

	public const int iCRI_HITSE_HT_BOM02 = 8;

	public const int iCRI_HITSE_HT_TRW01 = 10;

	public const int iCRI_HITSE_HT_TRW02 = 11;

	public const int iCRI_HITSE_HT_TRW03 = 12;

	public const int iCRI_HITSE_HT_FIRE = 13;

	public const int iCRI_HITSE_HT_COLD = 14;

	public const int iCRI_HITSE_HT_POISON = 15;

	public const int iCRI_HITSE_HT_ELECTRIC = 16;

	public const int iCRI_HITSE_HT_ENERGY = 17;

	public const int iCRI_HITSE_HT_EVIL = 20;

	public const int iCRI_HITSE_HT_SAND = 18;

	public const int iCRI_HITSE_HT_SKILL = 19;

	public const int iCRI_HITSE_HT_GUARD01 = 51;

	public const int iCRI_HITSE_HT_GUARD02 = 52;

	public const int iCRI_HITSE_HT_GUARD03 = 53;

	public const int iCRI_HITSE_HT_GUARD04 = 54;

	public const int iCRI_HITSE_HT_GUARD05 = 55;

	public const int iCRI_HITSE_HT_GUARD06 = 56;

	public const int iCRI_HITSE_HT_DEAD01 = 101;

	public const int iCRI_HITSE_HT_DEAD02 = 102;

	public const int iCRI_HITSE_HT_DEAD03 = 103;

	public const int iCRI_HITSE_HT_DEAD04 = 104;

	public const int iCRI_HITSE_HT_DEAD05 = 105;

	public const int iCRI_HITSE_HT_DEAD06 = 106;

	public const int iCRI_HITSE_HT_DEAD07 = 107;

	public const int iCRI_HITSE_HT_PLAYER01 = 71;

	public const int iCRI_HITSE_HT_WATER = 21;

	public const int iCRI_HITSE_HT_BOM03 = 9;

	public const int iCRI_HITSE_HT_SPLASH = 22;

	public const int iCRI_HITSE_HT_RIDEARMOR = 72;

	public const HitSE CRI_HITSE_CUENUM = HitSE.CRI_HITSE_CUENUM;

	public const HitSE CRI_HITSE_HT_SHOT02 = HitSE.CRI_HITSE_HT_SHOT02;

	public const HitSE CRI_HITSE_HT_SHOT01 = HitSE.CRI_HITSE_HT_SHOT01;

	public const HitSE CRI_HITSE_HT_SHOT03 = HitSE.CRI_HITSE_HT_SHOT03;

	public const HitSE CRI_HITSE_HT_CUT01 = HitSE.CRI_HITSE_HT_CUT01;

	public const HitSE CRI_HITSE_HT_CUT02 = HitSE.CRI_HITSE_HT_CUT02;

	public const HitSE CRI_HITSE_HT_CUT03 = HitSE.CRI_HITSE_HT_CUT03;

	public const HitSE CRI_HITSE_HT_BOM01 = HitSE.CRI_HITSE_HT_BOM01;

	public const HitSE CRI_HITSE_HT_BOM02 = HitSE.CRI_HITSE_HT_BOM02;

	public const HitSE CRI_HITSE_HT_TRW01 = HitSE.CRI_HITSE_HT_TRW01;

	public const HitSE CRI_HITSE_HT_TRW02 = HitSE.CRI_HITSE_HT_TRW02;

	public const HitSE CRI_HITSE_HT_TRW03 = HitSE.CRI_HITSE_HT_TRW03;

	public const HitSE CRI_HITSE_HT_FIRE = HitSE.CRI_HITSE_HT_FIRE;

	public const HitSE CRI_HITSE_HT_COLD = HitSE.CRI_HITSE_HT_COLD;

	public const HitSE CRI_HITSE_HT_POISON = HitSE.CRI_HITSE_HT_POISON;

	public const HitSE CRI_HITSE_HT_ELECTRIC = HitSE.CRI_HITSE_HT_ELECTRIC;

	public const HitSE CRI_HITSE_HT_ENERGY = HitSE.CRI_HITSE_HT_ENERGY;

	public const HitSE CRI_HITSE_HT_EVIL = HitSE.CRI_HITSE_HT_EVIL;

	public const HitSE CRI_HITSE_HT_SAND = HitSE.CRI_HITSE_HT_SAND;

	public const HitSE CRI_HITSE_HT_SKILL = HitSE.CRI_HITSE_HT_SKILL;

	public const HitSE CRI_HITSE_HT_GUARD01 = HitSE.CRI_HITSE_HT_GUARD01;

	public const HitSE CRI_HITSE_HT_GUARD02 = HitSE.CRI_HITSE_HT_GUARD02;

	public const HitSE CRI_HITSE_HT_GUARD03 = HitSE.CRI_HITSE_HT_GUARD03;

	public const HitSE CRI_HITSE_HT_GUARD04 = HitSE.CRI_HITSE_HT_GUARD04;

	public const HitSE CRI_HITSE_HT_GUARD05 = HitSE.CRI_HITSE_HT_GUARD05;

	public const HitSE CRI_HITSE_HT_GUARD06 = HitSE.CRI_HITSE_HT_GUARD06;

	public const HitSE CRI_HITSE_HT_DEAD01 = HitSE.CRI_HITSE_HT_DEAD01;

	public const HitSE CRI_HITSE_HT_DEAD02 = HitSE.CRI_HITSE_HT_DEAD02;

	public const HitSE CRI_HITSE_HT_DEAD03 = HitSE.CRI_HITSE_HT_DEAD03;

	public const HitSE CRI_HITSE_HT_DEAD04 = HitSE.CRI_HITSE_HT_DEAD04;

	public const HitSE CRI_HITSE_HT_DEAD05 = HitSE.CRI_HITSE_HT_DEAD05;

	public const HitSE CRI_HITSE_HT_DEAD06 = HitSE.CRI_HITSE_HT_DEAD06;

	public const HitSE CRI_HITSE_HT_DEAD07 = HitSE.CRI_HITSE_HT_DEAD07;

	public const HitSE CRI_HITSE_HT_PLAYER01 = HitSE.CRI_HITSE_HT_PLAYER01;

	public const HitSE CRI_HITSE_HT_WATER = HitSE.CRI_HITSE_HT_WATER;

	public const HitSE CRI_HITSE_HT_BOM03 = HitSE.CRI_HITSE_HT_BOM03;

	public const HitSE CRI_HITSE_HT_SPLASH = HitSE.CRI_HITSE_HT_SPLASH;

	public const HitSE CRI_HITSE_HT_RIDEARMOR = HitSE.CRI_HITSE_HT_RIDEARMOR;
}
