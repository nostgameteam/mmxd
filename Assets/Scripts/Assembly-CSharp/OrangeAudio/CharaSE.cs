namespace OrangeAudio
{
	public enum CharaSE : uint
	{
		JUMP = 1u,
		JUMPHIGH = 2u,
		CHAKUCHI = 3u,
		DASH = 4u,
		DASHEND = 5u,
		KABEPETA = 6u,
		KABEKERI = 7u,
		TELEPORTIN = 8u,
		TELEPORTOUT = 9u,
		STEP = 10u
	}
}
