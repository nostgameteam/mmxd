namespace OrangeAudio
{
	public enum Voice : uint
	{
		HURT1 = 1u,
		START1 = 2u,
		WIN1 = 3u,
		LOSE1 = 5u,
		UNIQUE1 = 6u,
		SKILL1 = 8u,
		SKILL2 = 9u,
		SKILL3 = 10u,
		JUMP1 = 12u,
		SABER1 = 14u,
		SABER2 = 15u,
		SABER3 = 16u,
		SABER4 = 17u,
		SABER5 = 18u,
		PINCH1 = 20u,
		PINCH2 = 21u,
		PINCH3 = 22u,
		ATTACK1 = 25u
	}
}
