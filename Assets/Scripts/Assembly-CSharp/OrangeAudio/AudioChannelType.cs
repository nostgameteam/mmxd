namespace OrangeAudio
{
	public enum AudioChannelType
	{
		BGM = 1,
		Sound = 2,
		Voice = 3
	}
}
