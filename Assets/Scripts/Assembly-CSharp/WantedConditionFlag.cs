public enum WantedConditionFlag : short
{
	None = 0,
	BasicCondition = 1,
	BonusCondition1 = 2,
	BonusCondition2 = 4,
	BonusCondition3 = 8
}
