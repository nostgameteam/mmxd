using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastController : MonoBehaviour
{
	public struct RaycastOrigins
	{
		public Vector2 topLeft;

		public Vector2 topRight;

		public Vector2 bottomLeft;

		public Vector2 bottomRight;
	}

	public LayerMask collisionMask;

	public const float skinWidth = 0.015f;

	public int horizontalRayCount = 4;

	public int verticalRayCount = 4;

	[HideInInspector]
	public float horizontalRaySpacing;

	[HideInInspector]
	public float verticalRaySpacing;

	[HideInInspector]
	public BoxCollider2D collider;

	public RaycastOrigins raycastOrigins;

	public virtual void Start()
	{
		collider = GetComponent<BoxCollider2D>();
		CalculateRaySpacing();
	}

	public void UpdateRaycastOrigins()
	{
		Bounds bounds = collider.bounds;
		bounds.Expand(-0.03f);
		raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
	}

	public void CalculateRaySpacing()
	{
		Bounds bounds = collider.bounds;
		bounds.Expand(-0.03f);
		horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
		verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);
		horizontalRaySpacing = bounds.size.y / (float)(horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (float)(verticalRayCount - 1);
	}
}
