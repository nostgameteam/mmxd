public static class EnemySE02i
{
	public const int iCRI_ENEMYSE02_CUENUM = 36;

	public const int iCRI_ENEMYSE02_EM022_MONKEY01 = 1;

	public const int iCRI_ENEMYSE02_EM022_MONKEY02 = 2;

	public const int iCRI_ENEMYSE02_EM022_MONKEY03 = 7;

	public const int iCRI_ENEMYSE02_EM023_PROMINENCE = 3;

	public const int iCRI_ENEMYSE02_EM023_PROMINENCE_LP = 11;

	public const int iCRI_ENEMYSE02_EM023_PROMINENCE_STOP = 12;

	public const int iCRI_ENEMYSE02_EM024_CNDRIVER01 = 4;

	public const int iCRI_ENEMYSE02_EM024_CNDRIVER02 = 5;

	public const int iCRI_ENEMYSE02_EM025_SMSIGN = 6;

	public const int iCRI_ENEMYSE02_EM026_SPIKEMARU = 8;

	public const int iCRI_ENEMYSE02_EM027_DEATHGUARD_LP = 9;

	public const int iCRI_ENEMYSE02_EM027_DEATHGUARD_STOP = 10;

	public const int iCRI_ENEMYSE02_EM028_BAR = 28;

	public const int iCRI_ENEMYSE02_EM029_REFLAZER01 = 29;

	public const int iCRI_ENEMYSE02_EM029_REFLAZER02 = 30;

	public const int iCRI_ENEMYSE02_EM030_PUMPKIN01 = 19;

	public const int iCRI_ENEMYSE02_EM031_GHOST01 = 21;

	public const int iCRI_ENEMYSE02_EM032_KYUNBYUN = 27;

	public const int iCRI_ENEMYSE02_EM033_EAT01 = 13;

	public const int iCRI_ENEMYSE02_EM034_BALL01 = 14;

	public const int iCRI_ENEMYSE02_EM034_BALL03 = 16;

	public const int iCRI_ENEMYSE02_EM034_BALL04_LG = 17;

	public const int iCRI_ENEMYSE02_EM034_BALL04_STOP = 18;

	public const int iCRI_ENEMYSE02_EM034_BALL02 = 15;

	public const int iCRI_ENEMYSE02_EM035_KNOT = 20;

	public const int iCRI_ENEMYSE02_EM036_RIDE01 = 22;

	public const int iCRI_ENEMYSE02_EM036_RIDE02 = 23;

	public const int iCRI_ENEMYSE02_EM036_RIDE03 = 24;

	public const int iCRI_ENEMYSE02_EM036_RIDE04 = 25;

	public const int iCRI_ENEMYSE02_EM036_RIDE05 = 26;

	public const int iCRI_ENEMYSE02_EM037_DARK01 = 31;

	public const int iCRI_ENEMYSE02_EM024_CNDRIVER03 = 35;

	public const int iCRI_ENEMYSE02_EM038_LAZER = 36;

	public const int iCRI_ENEMYSE02_EM039_CTBOMB01 = 38;

	public const int iCRI_ENEMYSE02_EM039_CTBOMB02 = 39;

	public const int iCRI_ENEMYSE02_EM039_CTBOMB03 = 40;

	public const EnemySE02 CRI_ENEMYSE02_CUENUM = EnemySE02.CRI_ENEMYSE02_CUENUM;

	public const EnemySE02 CRI_ENEMYSE02_EM022_MONKEY01 = EnemySE02.CRI_ENEMYSE02_EM022_MONKEY01;

	public const EnemySE02 CRI_ENEMYSE02_EM022_MONKEY02 = EnemySE02.CRI_ENEMYSE02_EM022_MONKEY02;

	public const EnemySE02 CRI_ENEMYSE02_EM022_MONKEY03 = EnemySE02.CRI_ENEMYSE02_EM022_MONKEY03;

	public const EnemySE02 CRI_ENEMYSE02_EM023_PROMINENCE = EnemySE02.CRI_ENEMYSE02_EM023_PROMINENCE;

	public const EnemySE02 CRI_ENEMYSE02_EM023_PROMINENCE_LP = EnemySE02.CRI_ENEMYSE02_EM023_PROMINENCE_LP;

	public const EnemySE02 CRI_ENEMYSE02_EM023_PROMINENCE_STOP = EnemySE02.CRI_ENEMYSE02_EM023_PROMINENCE_STOP;

	public const EnemySE02 CRI_ENEMYSE02_EM024_CNDRIVER01 = EnemySE02.CRI_ENEMYSE02_EM024_CNDRIVER01;

	public const EnemySE02 CRI_ENEMYSE02_EM024_CNDRIVER02 = EnemySE02.CRI_ENEMYSE02_EM024_CNDRIVER02;

	public const EnemySE02 CRI_ENEMYSE02_EM025_SMSIGN = EnemySE02.CRI_ENEMYSE02_EM025_SMSIGN;

	public const EnemySE02 CRI_ENEMYSE02_EM026_SPIKEMARU = EnemySE02.CRI_ENEMYSE02_EM026_SPIKEMARU;

	public const EnemySE02 CRI_ENEMYSE02_EM027_DEATHGUARD_LP = EnemySE02.CRI_ENEMYSE02_EM027_DEATHGUARD_LP;

	public const EnemySE02 CRI_ENEMYSE02_EM027_DEATHGUARD_STOP = EnemySE02.CRI_ENEMYSE02_EM027_DEATHGUARD_STOP;

	public const EnemySE02 CRI_ENEMYSE02_EM028_BAR = EnemySE02.CRI_ENEMYSE02_EM028_BAR;

	public const EnemySE02 CRI_ENEMYSE02_EM029_REFLAZER01 = EnemySE02.CRI_ENEMYSE02_EM029_REFLAZER01;

	public const EnemySE02 CRI_ENEMYSE02_EM029_REFLAZER02 = EnemySE02.CRI_ENEMYSE02_EM029_REFLAZER02;

	public const EnemySE02 CRI_ENEMYSE02_EM030_PUMPKIN01 = EnemySE02.CRI_ENEMYSE02_EM030_PUMPKIN01;

	public const EnemySE02 CRI_ENEMYSE02_EM031_GHOST01 = EnemySE02.CRI_ENEMYSE02_EM031_GHOST01;

	public const EnemySE02 CRI_ENEMYSE02_EM032_KYUNBYUN = EnemySE02.CRI_ENEMYSE02_EM032_KYUNBYUN;

	public const EnemySE02 CRI_ENEMYSE02_EM033_EAT01 = EnemySE02.CRI_ENEMYSE02_EM033_EAT01;

	public const EnemySE02 CRI_ENEMYSE02_EM034_BALL01 = EnemySE02.CRI_ENEMYSE02_EM034_BALL01;

	public const EnemySE02 CRI_ENEMYSE02_EM034_BALL03 = EnemySE02.CRI_ENEMYSE02_EM034_BALL03;

	public const EnemySE02 CRI_ENEMYSE02_EM034_BALL04_LG = EnemySE02.CRI_ENEMYSE02_EM034_BALL04_LG;

	public const EnemySE02 CRI_ENEMYSE02_EM034_BALL04_STOP = EnemySE02.CRI_ENEMYSE02_EM034_BALL04_STOP;

	public const EnemySE02 CRI_ENEMYSE02_EM034_BALL02 = EnemySE02.CRI_ENEMYSE02_EM034_BALL02;

	public const EnemySE02 CRI_ENEMYSE02_EM035_KNOT = EnemySE02.CRI_ENEMYSE02_EM035_KNOT;

	public const EnemySE02 CRI_ENEMYSE02_EM036_RIDE01 = EnemySE02.CRI_ENEMYSE02_EM036_RIDE01;

	public const EnemySE02 CRI_ENEMYSE02_EM036_RIDE02 = EnemySE02.CRI_ENEMYSE02_EM036_RIDE02;

	public const EnemySE02 CRI_ENEMYSE02_EM036_RIDE03 = EnemySE02.CRI_ENEMYSE02_EM036_RIDE03;

	public const EnemySE02 CRI_ENEMYSE02_EM036_RIDE04 = EnemySE02.CRI_ENEMYSE02_EM036_RIDE04;

	public const EnemySE02 CRI_ENEMYSE02_EM036_RIDE05 = EnemySE02.CRI_ENEMYSE02_EM036_RIDE05;

	public const EnemySE02 CRI_ENEMYSE02_EM037_DARK01 = EnemySE02.CRI_ENEMYSE02_EM037_DARK01;

	public const EnemySE02 CRI_ENEMYSE02_EM024_CNDRIVER03 = EnemySE02.CRI_ENEMYSE02_EM024_CNDRIVER03;

	public const EnemySE02 CRI_ENEMYSE02_EM038_LAZER = EnemySE02.CRI_ENEMYSE02_CUENUM;

	public const EnemySE02 CRI_ENEMYSE02_EM039_CTBOMB01 = EnemySE02.CRI_ENEMYSE02_EM039_CTBOMB01;

	public const EnemySE02 CRI_ENEMYSE02_EM039_CTBOMB02 = EnemySE02.CRI_ENEMYSE02_EM039_CTBOMB02;

	public const EnemySE02 CRI_ENEMYSE02_EM039_CTBOMB03 = EnemySE02.CRI_ENEMYSE02_EM039_CTBOMB03;
}
