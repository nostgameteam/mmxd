public enum SkillSE_SIGMA
{
	NONE = 0,
	CRI_SKILLSE_SIGMA_CUENUM = 16,
	CRI_SKILLSE_SIGMA_SG_CHARA01 = 40,
	CRI_SKILLSE_SIGMA_SG_CHARA03 = 42,
	CRI_SKILLSE_SIGMA_SG_START02 = 80,
	CRI_SKILLSE_SIGMA_SG_CHARA02 = 41,
	CRI_SKILLSE_SIGMA_SG_BLADE01 = 1,
	CRI_SKILLSE_SIGMA_SG_MAGNUM01 = 3,
	CRI_SKILLSE_SIGMA_SG_BLADE02 = 2,
	CRI_SKILLSE_SIGMA_SG_BAT = 4,
	CRI_SKILLSE_SIGMA_SG_ONIBI01_LP = 5,
	CRI_SKILLSE_SIGMA_SG_ONIBI01_STOP = 6,
	CRI_SKILLSE_SIGMA_SG_COMBO01 = 13,
	CRI_SKILLSE_SIGMA_SG_DUEL03 = 11,
	CRI_SKILLSE_SIGMA_SG_DUEL01 = 8,
	CRI_SKILLSE_SIGMA_SG_DUEL02_LP = 9,
	CRI_SKILLSE_SIGMA_SG_DUEL02_STOP = 10,
	CRI_SKILLSE_SIGMA_SG_COMBO02 = 14
}
