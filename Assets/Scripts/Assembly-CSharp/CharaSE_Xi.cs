public static class CharaSE_Xi
{
	public const int iCRI_CHARASE_X_CUENUM = 10;

	public const int iCRI_CHARASE_X_X_JUMP = 1;

	public const int iCRI_CHARASE_X_X_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_X_X_CHAKUCHI = 3;

	public const int iCRI_CHARASE_X_X_DASH = 4;

	public const int iCRI_CHARASE_X_X_DASHEND = 5;

	public const int iCRI_CHARASE_X_X_KABEPETA = 6;

	public const int iCRI_CHARASE_X_X_KABEKERI = 7;

	public const int iCRI_CHARASE_X_X_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_X_X_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_X_X_FOOT = 10;

	public const CharaSE_X CRI_CHARASE_X_CUENUM = CharaSE_X.CRI_CHARASE_X_CUENUM;

	public const CharaSE_X CRI_CHARASE_X_X_JUMP = CharaSE_X.CRI_CHARASE_X_X_JUMP;

	public const CharaSE_X CRI_CHARASE_X_X_JUMP_HIGH = CharaSE_X.CRI_CHARASE_X_X_JUMP_HIGH;

	public const CharaSE_X CRI_CHARASE_X_X_CHAKUCHI = CharaSE_X.CRI_CHARASE_X_X_CHAKUCHI;

	public const CharaSE_X CRI_CHARASE_X_X_DASH = CharaSE_X.CRI_CHARASE_X_X_DASH;

	public const CharaSE_X CRI_CHARASE_X_X_DASHEND = CharaSE_X.CRI_CHARASE_X_X_DASHEND;

	public const CharaSE_X CRI_CHARASE_X_X_KABEPETA = CharaSE_X.CRI_CHARASE_X_X_KABEPETA;

	public const CharaSE_X CRI_CHARASE_X_X_KABEKERI = CharaSE_X.CRI_CHARASE_X_X_KABEKERI;

	public const CharaSE_X CRI_CHARASE_X_X_TELEPORT_IN = CharaSE_X.CRI_CHARASE_X_X_TELEPORT_IN;

	public const CharaSE_X CRI_CHARASE_X_X_TELEPORT_OUT = CharaSE_X.CRI_CHARASE_X_X_TELEPORT_OUT;

	public const CharaSE_X CRI_CHARASE_X_X_FOOT = CharaSE_X.CRI_CHARASE_X_CUENUM;
}
