using System.ComponentModel;
using UnityEngine;

public class AotTypes : MonoBehaviour
{
	private static Int64Converter _unused = new Int64Converter();

	private static DecimalConverter _unused2 = new DecimalConverter();

	private static ByteConverter _unused3 = new ByteConverter();

	private static CollectionConverter _unused4 = new CollectionConverter();

	private static CharConverter _unused5 = new CharConverter();

	private static SByteConverter _unused6 = new SByteConverter();

	private static Int16Converter _unused7 = new Int16Converter();

	private static UInt16Converter _unused8 = new UInt16Converter();

	private static Int32Converter _unused9 = new Int32Converter();

	private static UInt32Converter _unused10 = new UInt32Converter();

	private static Int64Converter _unused11 = new Int64Converter();

	private static UInt64Converter _unused12 = new UInt64Converter();

	private static DoubleConverter _unused13 = new DoubleConverter();

	private static SingleConverter _unused14 = new SingleConverter();

	private static BooleanConverter _unused15 = new BooleanConverter();

	private static StringConverter _unused16 = new StringConverter();

	private static DateTimeConverter _unused17 = new DateTimeConverter();

	private static TimeSpanConverter _unused18 = new TimeSpanConverter();

	private static OrangeDataManager _unused19 = new OrangeDataManager();

	private static OrangeTextDataManager _unused20 = new OrangeTextDataManager();
}
