public enum GamepadType
{
	Unknown = 0,
	Xbox = 10,
	XboxOne = 11,
	PS4_Wired = 21,
	PS4_Wireless = 22,
	NSPro = 30,
	XInput = 40
}
