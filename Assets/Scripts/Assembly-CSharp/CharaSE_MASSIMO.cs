public enum CharaSE_MASSIMO
{
	NONE = 0,
	CRI_CHARASE_MASSIMO_CUENUM = 10,
	CRI_CHARASE_MASSIMO_MS_JUMP = 1,
	CRI_CHARASE_MASSIMO_MS_JUMP_HIGH = 2,
	CRI_CHARASE_MASSIMO_MS_CHAKUCHI = 3,
	CRI_CHARASE_MASSIMO_MS_DASH = 4,
	CRI_CHARASE_MASSIMO_MS_DASHEND = 5,
	CRI_CHARASE_MASSIMO_MS_KABEPETA = 6,
	CRI_CHARASE_MASSIMO_MS_KABEKERI = 7,
	CRI_CHARASE_MASSIMO_MS_TELEPORT_IN = 8,
	CRI_CHARASE_MASSIMO_MS_TELEPORT_OUT = 9,
	CRI_CHARASE_MASSIMO_MS_FOOT = 10
}
