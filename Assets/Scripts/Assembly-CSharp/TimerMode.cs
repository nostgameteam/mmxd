public enum TimerMode
{
	MILLISECOND = 0,
	FRAME = 1,
	TICKS = 2
}
