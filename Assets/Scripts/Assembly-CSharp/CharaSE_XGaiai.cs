public static class CharaSE_XGaiai
{
	public const int iCRI_CHARASE_XGAIA_CUENUM = 10;

	public const int iCRI_CHARASE_XGAIA_XG_JUMP = 1;

	public const int iCRI_CHARASE_XGAIA_XG_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_XGAIA_XG_CHAKUCHI = 3;

	public const int iCRI_CHARASE_XGAIA_XG_DASH = 4;

	public const int iCRI_CHARASE_XGAIA_XG_DASHEND = 5;

	public const int iCRI_CHARASE_XGAIA_XG_KABEPETA = 6;

	public const int iCRI_CHARASE_XGAIA_XG_KABEKERI = 7;

	public const int iCRI_CHARASE_XGAIA_XG_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_XGAIA_XG_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_XGAIA_XG_FOOT = 10;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_CUENUM = CharaSE_XGaia.CRI_CHARASE_XGAIA_CUENUM;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_JUMP = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_JUMP;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_JUMP_HIGH = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_JUMP_HIGH;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_CHAKUCHI = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_CHAKUCHI;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_DASH = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_DASH;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_DASHEND = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_DASHEND;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_KABEPETA = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_KABEPETA;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_KABEKERI = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_KABEKERI;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_TELEPORT_IN = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_TELEPORT_IN;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_TELEPORT_OUT = CharaSE_XGaia.CRI_CHARASE_XGAIA_XG_TELEPORT_OUT;

	public const CharaSE_XGaia CRI_CHARASE_XGAIA_XG_FOOT = CharaSE_XGaia.CRI_CHARASE_XGAIA_CUENUM;
}
