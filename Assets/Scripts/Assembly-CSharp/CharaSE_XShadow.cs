public enum CharaSE_XShadow
{
	NONE = 0,
	CRI_CHARASE_XSHADOW_CUENUM = 10,
	CRI_CHARASE_XSHADOW_XS_JUMP = 1,
	CRI_CHARASE_XSHADOW_XS_JUMP_HIGH = 2,
	CRI_CHARASE_XSHADOW_XS_CHAKUCHI = 3,
	CRI_CHARASE_XSHADOW_XS_DASH = 4,
	CRI_CHARASE_XSHADOW_XS_DASHEND = 5,
	CRI_CHARASE_XSHADOW_XS_KABEPETA = 6,
	CRI_CHARASE_XSHADOW_XS_KABEKERI = 7,
	CRI_CHARASE_XSHADOW_XS_TELEPORT_IN = 8,
	CRI_CHARASE_XSHADOW_XS_TELEPORT_OUT = 9,
	CRI_CHARASE_XSHADOW_XS_FOOT = 10
}
