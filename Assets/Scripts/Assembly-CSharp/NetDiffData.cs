using JsonFx.Json;

public class NetDiffData
{
	[JsonName("a")]
	public int key;

	[JsonName("b")]
	public object value;
}
