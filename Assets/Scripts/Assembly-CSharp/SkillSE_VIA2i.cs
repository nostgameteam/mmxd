public static class SkillSE_VIA2i
{
	public const int iCRI_SKILLSE_VIA2_CUENUM = 7;

	public const int iCRI_SKILLSE_VIA2_VI2_DOOM02 = 5;

	public const int iCRI_SKILLSE_VIA2_VI2_DOOM01 = 4;

	public const int iCRI_SKILLSE_VIA2_VI2_ZAN02 = 2;

	public const int iCRI_SKILLSE_VIA2_VI2_CHARA01 = 40;

	public const int iCRI_SKILLSE_VIA2_VI2_START01 = 80;

	public const int iCRI_SKILLSE_VIA2_VI2_ZAN01 = 1;

	public const int iCRI_SKILLSE_VIA2_VI2_ZAN03 = 3;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_CUENUM = SkillSE_VIA2.CRI_SKILLSE_VIA2_CUENUM;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_DOOM02 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_DOOM02;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_DOOM01 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_DOOM01;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_ZAN02 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_ZAN02;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_CHARA01 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_CHARA01;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_START01 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_START01;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_ZAN01 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_ZAN01;

	public const SkillSE_VIA2 CRI_SKILLSE_VIA2_VI2_ZAN03 = SkillSE_VIA2.CRI_SKILLSE_VIA2_VI2_ZAN03;
}
