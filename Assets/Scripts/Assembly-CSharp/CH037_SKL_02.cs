internal class CH037_SKL_02
{
	public static int FRAME_TRIGGER = (int)(0.295f / GameLogicUpdateManager.m_fFrameLen);

	public static int FRAME_END = (int)(0.71f / GameLogicUpdateManager.m_fFrameLen);

	public const int CREATE_COUNT = 8;

	public const int INCREASE_FRAME = 1;

	public const string TRANSFORM_NAME = "Sub_Tron_ShootPoint2";
}
