public class SocketFriendInviteReceiveInfo
{
	public string TargetPlayerID = string.Empty;

	public string InviteMessage = string.Empty;

	public string FriendPlayerHUD = string.Empty;

	public int Busy;
}
