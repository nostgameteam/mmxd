namespace OrangeCriRelay
{
	public enum AudioType
	{
		NONE = 0,
		VOICE = 1,
		SKILL = 2,
		CHARACTER = 3,
		TELEPORT = 4,
		CHARGE_SHOT = 5,
		CALL_PET = 6
	}
}
