using enums;

public class AccountInfo
{
	public string ID;

	public string Secret;

	public AccountSourceType SourceType = AccountSourceType.Unity;
}
