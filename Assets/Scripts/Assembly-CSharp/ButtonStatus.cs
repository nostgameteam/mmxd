public enum ButtonStatus
{
	NONE = 0,
	PRESSED = 1,
	HELD = 2,
	RELEASED = 3
}
