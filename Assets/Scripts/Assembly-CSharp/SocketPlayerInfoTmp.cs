public struct SocketPlayerInfoTmp
{
	public string PlayerHUD { get; set; }

	public string InfoJSON { get; set; }

	public string CharJSON { get; set; }

	public string WeaponJSON { get; set; }

	public string ChipJSON { get; set; }

	public string EquipJSON { get; set; }

	public string FinalStrikeJSON { get; set; }

	public string TitleJSON { get; set; }
}
