public abstract class GuildUIBase : OrangeUIBase
{
	protected virtual void OnEnable()
	{
	}

	protected virtual void OnDisable()
	{
	}
}
