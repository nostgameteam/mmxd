public static class BGMe5i
{
	public const int iCRI_BGME5_CUENUM = 1;

	public const int iCRI_BGME5_BGM_ST_E5B = 2;

	public const BGMe5 CRI_BGME5_CUENUM = BGMe5.CRI_BGME5_CUENUM;

	public const BGMe5 CRI_BGME5_BGM_ST_E5B = BGMe5.CRI_BGME5_BGM_ST_E5B;
}
