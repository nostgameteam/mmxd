public static class STORY_IRISi
{
	public const int iCRI_STORY_IRIS_CUENUM = 27;

	public const int iCRI_STORY_IRIS_S_IR_STORY01 = 1;

	public const int iCRI_STORY_IRIS_S_IR_STORY02 = 2;

	public const int iCRI_STORY_IRIS_S_IR_STORY03 = 3;

	public const int iCRI_STORY_IRIS_S_IR_STORY04 = 4;

	public const int iCRI_STORY_IRIS_S_IR_STORY05 = 5;

	public const int iCRI_STORY_IRIS_S_IR_STORY06 = 6;

	public const int iCRI_STORY_IRIS_S_IR_STORY07 = 7;

	public const int iCRI_STORY_IRIS_S_IR_STORY08 = 8;

	public const int iCRI_STORY_IRIS_S_IR_STORY09 = 9;

	public const int iCRI_STORY_IRIS_S_IR_STORY10 = 10;

	public const int iCRI_STORY_IRIS_S_IR_STORY11 = 11;

	public const int iCRI_STORY_IRIS_S_IR_STORY12 = 12;

	public const int iCRI_STORY_IRIS_S_IR_STORY13 = 13;

	public const int iCRI_STORY_IRIS_S_IR_STORY14 = 14;

	public const int iCRI_STORY_IRIS_S_IR_STORY15 = 15;

	public const int iCRI_STORY_IRIS_S_IR_STORY16 = 16;

	public const int iCRI_STORY_IRIS_S_IR_STORY17 = 17;

	public const int iCRI_STORY_IRIS_S_IR_STORY18 = 18;

	public const int iCRI_STORY_IRIS_S_IR_STORY19 = 19;

	public const int iCRI_STORY_IRIS_S_IR_STORY20 = 20;

	public const int iCRI_STORY_IRIS_S_IR_STORY21 = 21;

	public const int iCRI_STORY_IRIS_S_IR_STORY22 = 22;

	public const int iCRI_STORY_IRIS_S_IR_STORY23 = 23;

	public const int iCRI_STORY_IRIS_S_IR_STORY24 = 24;

	public const int iCRI_STORY_IRIS_S_IR_STORY25 = 25;

	public const int iCRI_STORY_IRIS_S_IR_STORY26 = 26;

	public const int iCRI_STORY_IRIS_S_IR_STORY27 = 27;

	public const STORY_IRIS CRI_STORY_IRIS_CUENUM = STORY_IRIS.CRI_STORY_IRIS_CUENUM;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY01 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY01;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY02 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY02;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY03 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY03;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY04 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY04;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY05 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY05;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY06 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY06;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY07 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY07;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY08 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY08;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY09 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY09;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY10 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY10;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY11 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY11;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY12 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY12;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY13 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY13;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY14 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY14;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY15 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY15;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY16 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY16;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY17 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY17;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY18 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY18;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY19 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY19;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY20 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY20;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY21 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY21;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY22 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY22;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY23 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY23;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY24 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY24;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY25 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY25;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY26 = STORY_IRIS.CRI_STORY_IRIS_S_IR_STORY26;

	public const STORY_IRIS CRI_STORY_IRIS_S_IR_STORY27 = STORY_IRIS.CRI_STORY_IRIS_CUENUM;
}
