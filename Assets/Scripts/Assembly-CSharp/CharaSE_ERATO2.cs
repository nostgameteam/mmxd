public enum CharaSE_ERATO2
{
	NONE = 0,
	CRI_CHARASE_ERATO2_CUENUM = 10,
	CRI_CHARASE_ERATO2_ER2_JUMP = 1,
	CRI_CHARASE_ERATO2_ER2_JUMP_HIGH = 2,
	CRI_CHARASE_ERATO2_ER2_CHAKUCHI = 3,
	CRI_CHARASE_ERATO2_ER2_DASH = 4,
	CRI_CHARASE_ERATO2_ER2_DASHEND = 5,
	CRI_CHARASE_ERATO2_ER2_KABEPETA = 6,
	CRI_CHARASE_ERATO2_ER2_KABEKERI = 7,
	CRI_CHARASE_ERATO2_ER2_TELEPORT_IN = 8,
	CRI_CHARASE_ERATO2_ER2_TELEPORT_OUT = 9,
	CRI_CHARASE_ERATO2_ER2_FOOT = 10
}
