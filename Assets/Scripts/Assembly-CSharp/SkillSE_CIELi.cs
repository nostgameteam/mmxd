public static class SkillSE_CIELi
{
	public const int iCRI_SKILLSE_CIEL_CUENUM = 22;

	public const int iCRI_SKILLSE_CIEL_CL_ELF01_LP = 1;

	public const int iCRI_SKILLSE_CIEL_CL_ELFT03 = 15;

	public const int iCRI_SKILLSE_CIEL_CL_ELFT02_STOP = 14;

	public const int iCRI_SKILLSE_CIEL_CL_ELFT02_LP = 13;

	public const int iCRI_SKILLSE_CIEL_CL_ELFT01 = 12;

	public const int iCRI_SKILLSE_CIEL_CL_ELFX03 = 20;

	public const int iCRI_SKILLSE_CIEL_CL_ELFX02_STOP = 19;

	public const int iCRI_SKILLSE_CIEL_CL_ELFX02_LP = 18;

	public const int iCRI_SKILLSE_CIEL_CL_ELFX01 = 17;

	public const int iCRI_SKILLSE_CIEL_CL_ELF01_STOP = 2;

	public const int iCRI_SKILLSE_CIEL_CL_ELF02 = 3;

	public const int iCRI_SKILLSE_CIEL_CL_SYSTEM01 = 4;

	public const int iCRI_SKILLSE_CIEL_CL_SYSTEM02_LP = 5;

	public const int iCRI_SKILLSE_CIEL_CL_SYSTEM02_STOP = 6;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA01 = 8;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA02 = 9;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA03 = 10;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA04 = 11;

	public const int iCRI_SKILLSE_CIEL_CL_CHOCO01 = 7;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA05 = 40;

	public const int iCRI_SKILLSE_CIEL_CL_CHARA06 = 41;

	public const int iCRI_SKILLSE_CIEL_CL_START06 = 80;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CUENUM = SkillSE_CIEL.CRI_SKILLSE_CIEL_CUENUM;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELF01_LP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELF01_LP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFT03 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFT03;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFT02_STOP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFT02_STOP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFT02_LP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFT02_LP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFT01 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFT01;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFX03 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFX03;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFX02_STOP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFX02_STOP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFX02_LP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFX02_LP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELFX01 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELFX01;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELF01_STOP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELF01_STOP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_ELF02 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_ELF02;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_SYSTEM01 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_SYSTEM01;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_SYSTEM02_LP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_SYSTEM02_LP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_SYSTEM02_STOP = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_SYSTEM02_STOP;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA01 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA01;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA02 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA02;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA03 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA03;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA04 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA04;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHOCO01 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHOCO01;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA05 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA05;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_CHARA06 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_CHARA06;

	public const SkillSE_CIEL CRI_SKILLSE_CIEL_CL_START06 = SkillSE_CIEL.CRI_SKILLSE_CIEL_CL_START06;
}
