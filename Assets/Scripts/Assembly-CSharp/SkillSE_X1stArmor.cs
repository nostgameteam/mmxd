public enum SkillSE_X1stArmor
{
	NONE = 0,
	CRI_SKILLSE_X1STARMOR_CUENUM = 7,
	CRI_SKILLSE_X1STARMOR_X1_SCBUSTER_S = 3,
	CRI_SKILLSE_X1STARMOR_X1_SCBUSTER_M = 4,
	CRI_SKILLSE_X1STARMOR_X1_SCBUSTER_L = 5,
	CRI_SKILLSE_X1STARMOR_X1_SCBUSTER_O = 6,
	CRI_SKILLSE_X1STARMOR_X1_CHARGE_LP = 1,
	CRI_SKILLSE_X1STARMOR_X1_CHARGE_STOP = 2,
	CRI_SKILLSE_X1STARMOR_X1_HEADCRUSH = 7
}
