public static class CharaSE_CH034_000i
{
	public const int iCRI_CHARASE_CH034_000_CUENUM = 10;

	public const int iCRI_CHARASE_CH034_000_CH034_JUMP = 1;

	public const int iCRI_CHARASE_CH034_000_CH034_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_CH034_000_CH034_CHAKUCHI = 3;

	public const int iCRI_CHARASE_CH034_000_CH034_DASH = 4;

	public const int iCRI_CHARASE_CH034_000_CH034_DASHEND = 5;

	public const int iCRI_CHARASE_CH034_000_CH034_KABEPETA = 6;

	public const int iCRI_CHARASE_CH034_000_CH034_KABEKERI = 7;

	public const int iCRI_CHARASE_CH034_000_CH034_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_CH034_000_CH034_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_CH034_000_CH034_FOOT = 10;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CUENUM = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CUENUM;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_JUMP = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_JUMP;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_JUMP_HIGH = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_JUMP_HIGH;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_CHAKUCHI = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_CHAKUCHI;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_DASH = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_DASH;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_DASHEND = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_DASHEND;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_KABEPETA = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_KABEPETA;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_KABEKERI = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_KABEKERI;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_TELEPORT_IN = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_TELEPORT_IN;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_TELEPORT_OUT = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CH034_TELEPORT_OUT;

	public const CharaSE_CH034_000 CRI_CHARASE_CH034_000_CH034_FOOT = CharaSE_CH034_000.CRI_CHARASE_CH034_000_CUENUM;
}
