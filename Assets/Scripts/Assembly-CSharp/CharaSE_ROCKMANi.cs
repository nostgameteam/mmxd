public static class CharaSE_ROCKMANi
{
	public const int iCRI_CHARASE_ROCKMAN_CUENUM = 10;

	public const int iCRI_CHARASE_ROCKMAN_RM_JUMP = 1;

	public const int iCRI_CHARASE_ROCKMAN_RM_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_ROCKMAN_RM_CHAKUCHI = 3;

	public const int iCRI_CHARASE_ROCKMAN_RM_DASH = 4;

	public const int iCRI_CHARASE_ROCKMAN_RM_DASHEND = 5;

	public const int iCRI_CHARASE_ROCKMAN_RM_KABEPETA = 6;

	public const int iCRI_CHARASE_ROCKMAN_RM_KABEKERI = 7;

	public const int iCRI_CHARASE_ROCKMAN_RM_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_ROCKMAN_RM_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_ROCKMAN_RM_FOOT = 10;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_CUENUM = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_CUENUM;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_JUMP = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_JUMP;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_JUMP_HIGH = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_JUMP_HIGH;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_CHAKUCHI = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_CHAKUCHI;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_DASH = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_DASH;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_DASHEND = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_DASHEND;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_KABEPETA = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_KABEPETA;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_KABEKERI = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_KABEKERI;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_TELEPORT_IN = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_TELEPORT_IN;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_TELEPORT_OUT = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_RM_TELEPORT_OUT;

	public const CharaSE_ROCKMAN CRI_CHARASE_ROCKMAN_RM_FOOT = CharaSE_ROCKMAN.CRI_CHARASE_ROCKMAN_CUENUM;
}
