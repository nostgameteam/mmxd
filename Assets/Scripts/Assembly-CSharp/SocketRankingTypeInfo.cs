using enums;

public class SocketRankingTypeInfo
{
	public RankType m_RankType;

	public int m_Rank;

	public int m_Score;

	public int m_TotalCount;

	public int m_ServerID;
}
