public class UINames
{
	public const string BG_Bag = "BG_Bag";

	public const string Bg_Black = "Bg_Black";

	public const string Bg_Black_Ex = "Bg_Black_Ex";

	public const string Bg_Bosschallenge = "Bg_Bosschallenge";

	public const string Bg_Bosschallenge_BG00 = "Bg_Bosschallenge_BG00";

	public const string Bg_Cashapon_BG_01 = "Bg_Cashapon_BG_01";

	public const string Bg_Cashapon_BG_02 = "Bg_Cashapon_BG_02";

	public const string Bg_CharacterBg = "Bg_CharacterBg";

	public const string Bg_CharacterMenu = "Bg_CharacterMenu";

	public const string Bg_ChipBg = "Bg_ChipBg";

	public const string Bg_Common = "Bg_Common";

	public const string Bg_Common_BG01 = "Bg_Common_BG01";

	public const string Bg_Common_BG02 = "Bg_Common_BG02";

	public const string Bg_Common_BG03 = "Bg_Common_BG03";

	public const string Bg_Common_BG04 = "Bg_Common_BG04";

	public const string Bg_Common_BG05 = "Bg_Common_BG05";

	public const string Bg_Common_BG06 = "Bg_Common_BG06";

	public const string Bg_Common_BG07 = "Bg_Common_BG07";

	public const string Bg_Common_Dummy = "Bg_Common_Dummy";

	public const string Bg_CoopRoom = "Bg_CoopRoom";

	public const string Bg_FightBg = "Bg_FightBg";

	public const string Bg_Gacha_Result = "Bg_Gacha_Result";

	public const string Bg_GuildLobby = "Bg_GuildLobby";

	public const string Bg_L10nRaw = "Bg_L10nRaw";

	public const string Bg_Lab = "Bg_Lab";

	public const string Bg_LabEvent_01 = "Bg_LabEvent_01";

	public const string Bg_Login_01 = "Bg_Login_01";

	public const string Bg_Login_reward = "Bg_Login_reward";

	public const string Bg_Multiplay = "Bg_Multiplay";

	public const string Bg_PowerGuide = "Bg_PowerGuide";

	public const string BG_PrizeWheel = "BG_PrizeWheel";

	public const string Bg_PVP_Bg = "Bg_PVP_Bg";

	public const string BG_PvpMatch = "BG_PvpMatch";

	public const string BG_PvpRank = "BG_PvpRank";

	public const string BG_Record = "BG_Record";

	public const string Bg_Setting = "Bg_Setting";

	public const string BG_Shop_BG = "BG_Shop_BG";

	public const string Bg_Stage_000 = "Bg_Stage_000";

	public const string Bg_Stage_001 = "Bg_Stage_001";

	public const string Bg_Stage_002 = "Bg_Stage_002";

	public const string Bg_Stage_003 = "Bg_Stage_003";

	public const string Bg_Stage_004 = "Bg_Stage_004";

	public const string Bg_Stage_005 = "Bg_Stage_005";

	public const string Bg_Stage_006 = "Bg_Stage_006";

	public const string Bg_Stage_007 = "Bg_Stage_007";

	public const string Bg_Stage_008 = "Bg_Stage_008";

	public const string Bg_Stage_009 = "Bg_Stage_009";

	public const string Bg_Stage_010 = "Bg_Stage_010";

	public const string Bg_Stage_011 = "Bg_Stage_011";

	public const string Bg_Stage_012 = "Bg_Stage_012";

	public const string Bg_Stage_013 = "Bg_Stage_013";

	public const string Bg_Stage_014 = "Bg_Stage_014";

	public const string Bg_Stage_016 = "Bg_Stage_016";

	public const string Bg_Stage_017 = "Bg_Stage_017";

	public const string Bg_Stage_018 = "Bg_Stage_018";

	public const string Bg_Stage_019 = "Bg_Stage_019";

	public const string Bg_Stage_020 = "Bg_Stage_020";

	public const string Bg_Stage_021 = "Bg_Stage_021";

	public const string Bg_Stage_022 = "Bg_Stage_022";

	public const string Bg_Tower_Stage_001 = "Bg_Tower_Stage_001";

	public const string Bg_Tower_Stage_002 = "Bg_Tower_Stage_002";

	public const string Bg_Tower_Stage_003 = "Bg_Tower_Stage_003";

	public const string Bg_Tower_Stage_004 = "Bg_Tower_Stage_004";

	public const string Bg_Tower_Stage_005 = "Bg_Tower_Stage_005";

	public const string Bg_Tower_Stage_006 = "Bg_Tower_Stage_006";

	public const string Bg_Tower_Stage_007 = "Bg_Tower_Stage_007";

	public const string Bg_Tower_Stage_008 = "Bg_Tower_Stage_008";

	public const string Bg_Tower_Stage_009 = "Bg_Tower_Stage_009";

	public const string Bg_Tower_Stage_010 = "Bg_Tower_Stage_010";

	public const string Bg_Tower_Stage_011 = "Bg_Tower_Stage_011";

	public const string Bg_Tower_Stage_012 = "Bg_Tower_Stage_012";

	public const string Bg_Tower_Stage_013 = "Bg_Tower_Stage_013";

	public const string Bg_Tower_Stage_014 = "Bg_Tower_Stage_014";

	public const string Bg_Tower_Stage_015 = "Bg_Tower_Stage_015";

	public const string Bg_Tower_Stage_016 = "Bg_Tower_Stage_016";

	public const string Bg_Tower_Stage_017 = "Bg_Tower_Stage_017";

	public const string Bg_Tower_Stage_018 = "Bg_Tower_Stage_018";

	public const string Bg_Tower_Stage_019 = "Bg_Tower_Stage_019";

	public const string Bg_Tower_Stage_020 = "Bg_Tower_Stage_020";

	public const string Bg_Tower_Stage_021 = "Bg_Tower_Stage_021";

	public const string Bg_Tower_Stage_022 = "Bg_Tower_Stage_022";

	public const string Bg_Tower_Stage_023 = "Bg_Tower_Stage_023";

	public const string Bg_Tower_Stage_024 = "Bg_Tower_Stage_024";

	public const string Bg_Tower_Stage_025 = "Bg_Tower_Stage_025";

	public const string Bg_Tower_Stage_026 = "Bg_Tower_Stage_026";

	public const string Bg_Tower_Stage_027 = "Bg_Tower_Stage_027";

	public const string Bg_Tower_Stage_028 = "Bg_Tower_Stage_028";

	public const string Bg_Tower_Stage_029 = "Bg_Tower_Stage_029";

	public const string Bg_Tower_Stage_031 = "Bg_Tower_Stage_031";

	public const string Bg_Tower_Stage_032 = "Bg_Tower_Stage_032";

	public const string Bg_Tower_Stage_033 = "Bg_Tower_Stage_033";

	public const string Bg_VPS = "Bg_VPS";

	public const string Bg_Wanted = "Bg_Wanted";

	public const string Bg_WeaponBg = "Bg_WeaponBg";

	public const string UI_Bossrush_BG_01 = "UI_Bossrush_BG_01";

	public const string UI_Bossrush_BG_02 = "UI_Bossrush_BG_02";

	public const string UI_Bossrush_BG_03 = "UI_Bossrush_BG_03";

	public const string UI_Bossrush_BG_04 = "UI_Bossrush_BG_04";

	public const string UI_Bossrush_BG_05 = "UI_Bossrush_BG_05";

	public const string UI_Bossrush_BG_06 = "UI_Bossrush_BG_06";

	public const string UI_Bossrush_BG_07 = "UI_Bossrush_BG_07";

	public const string UI_Bossrush_BG_08 = "UI_Bossrush_BG_08";

	public const string UI_Event_BG_01 = "UI_Event_BG_01";

	public const string UI_Event_BG_02 = "UI_Event_BG_02";

	public const string UI_Event_BG_03 = "UI_Event_BG_03";

	public const string UI_Event_BG_04 = "UI_Event_BG_04";

	public const string UI_Event_BG_05 = "UI_Event_BG_05";

	public const string UI_Event_BG_Black = "UI_Event_BG_Black";

	public const string UI_RaidBoss_BG_01 = "UI_RaidBoss_BG_01";

	public const string UI_RaidBoss_BG_02 = "UI_RaidBoss_BG_02";

	public const string UI_RaidBoss_BG_03 = "UI_RaidBoss_BG_03";

	public const string UI_RaidBoss_BG_04 = "UI_RaidBoss_BG_04";

	public const string UI_RaidBoss_BG_05 = "UI_RaidBoss_BG_05";

	public const string UI_RaidBoss_BG_06 = "UI_RaidBoss_BG_06";

	public const string UI_RaidBoss_BG_Common = "UI_RaidBoss_BG_Common";

	public const string UI_speedmode_BG_chall = "UI_speedmode_BG_chall";

	public const string UI_speedmode_BG_fire = "UI_speedmode_BG_fire";

	public const string UI_Speedrun_BG_01 = "UI_Speedrun_BG_01";

	public const string UI_SPEvent_BG_01 = "UI_SPEvent_BG_01";

	public const string UI_SPEvent_BG_02 = "UI_SPEvent_BG_02";

	public const string UI_SPEvent_BG_03 = "UI_SPEvent_BG_03";

	public const string UI_SPEvent_BG_04 = "UI_SPEvent_BG_04";

	public const string UI_SPEvent_BG_05 = "UI_SPEvent_BG_05";

	public const string UI_SPEvent_BG_06 = "UI_SPEvent_BG_06";

	public const string UI_SPEvent_BG_07 = "UI_SPEvent_BG_07";

	public const string UI_SPEvent_BG_08 = "UI_SPEvent_BG_08";

	public const string UI_SPEvent_BG_09 = "UI_SPEvent_BG_09";

	public const string UI_SPEvent_BG_10 = "UI_SPEvent_BG_10";

	public const string UI_SPEvent_BG_11 = "UI_SPEvent_BG_11";

	public const string UI_SPEvent_BG_12 = "UI_SPEvent_BG_12";

	public const string UI_SPEvent_BG_13 = "UI_SPEvent_BG_13";

	public const string UI_SPEvent_BG_14 = "UI_SPEvent_BG_14";

	public const string UI_SPEvent_BG_15 = "UI_SPEvent_BG_15";

	public const string UI_SPEvent_BG_16 = "UI_SPEvent_BG_16";

	public const string UI_SPEvent_BG_17 = "UI_SPEvent_BG_17";

	public const string UI_SPEvent_BG_18 = "UI_SPEvent_BG_18";

	public const string UI_SPEvent_BG_19 = "UI_SPEvent_BG_19";

	public const string UI_SPEvent_BG_20 = "UI_SPEvent_BG_20";

	public const string UI_SPEvent_BG_21 = "UI_SPEvent_BG_21";

	public const string UI_SPEvent_BG_22 = "UI_SPEvent_BG_22";

	public const string UI_SPEvent_BG_23 = "UI_SPEvent_BG_23";

	public const string UI_SPEvent_BG_24 = "UI_SPEvent_BG_24";

	public const string UI_SPEvent_BG_25 = "UI_SPEvent_BG_25";

	public const string UI_SPEvent_BG_26 = "UI_SPEvent_BG_26";

	public const string UI_SPEvent_BG_27 = "UI_SPEvent_BG_27";

	public const string UI_SPEvent_BG_28 = "UI_SPEvent_BG_28";

	public const string UI_SPEvent_BG_29 = "UI_SPEvent_BG_29";

	public const string UI_SPEvent_BG_30 = "UI_SPEvent_BG_30";

	public const string UI_SPEvent_BG_31 = "UI_SPEvent_BG_31";

	public const string UI_SPEvent_BG_32 = "UI_SPEvent_BG_32";

	public const string UI_SPEvent_BG_33 = "UI_SPEvent_BG_33";

	public const string UI_SPEvent_BG_34 = "UI_SPEvent_BG_34";

	public const string UI_SPEvent_BG_35 = "UI_SPEvent_BG_35";

	public const string UI_SPEvent_BG_36 = "UI_SPEvent_BG_36";

	public const string UI_TBattle_BG = "UI_TBattle_BG";

	public const string BattleScoreRoot0 = "BattleScoreRoot0";

	public const string BattleScoreRoot1 = "BattleScoreRoot1";

	public const string BattleScoreRoot2 = "BattleScoreRoot2";

	public const string BattleScoreRoot3 = "BattleScoreRoot3";

	public const string CharacterColumeSmall = "CharacterColumeSmall";

	public const string KillActivityRoot0 = "KillActivityRoot0";

	public const string KillActivityRoot1 = "KillActivityRoot1";

	public const string KillActivityRoot2 = "KillActivityRoot2";

	public const string ObjCountDownBar = "ObjCountDownBar";

	public const string ObjInfoBar = "ObjInfoBar";

	public const string StageRewardColume = "StageRewardColume";

	public const string UI_BattleInfo = "UI_BattleInfo";

	public const string UI_CtrlHint = "UI_CtrlHint";

	public const string UI_GoCheck = "UI_GoCheck";

	public const string UI_STAGEREWARD = "UI_STAGEREWARD";

	public const string Button_Shiny_1 = "Button_Shiny_1";

	public const string InputField_1 = "InputField_1";

	public const string ItemTitle_1 = "ItemTitle_1";

	public const string ItemTitle_2 = "ItemTitle_2";

	public const string PanelButtonConfirmCancel = "PanelButtonConfirmCancel";

	public const string PanelChildUIBackground = "PanelChildUIBackground";

	public const string PanelCountHint = "PanelCountHint";

	public const string PanelEmptyHint = "PanelEmptyHint";

	public const string PanelInputField_1 = "PanelInputField_1";

	public const string PanelInputField_2 = "PanelInputField_2";

	public const string PanelItemInfoBG = "PanelItemInfoBG";

	public const string PanelLimitHint = "PanelLimitHint";

	public const string PanelLoopHorizontalScrollRect = "PanelLoopHorizontalScrollRect";

	public const string PanelLoopVerticalScrollRect = "PanelLoopVerticalScrollRect";

	public const string PanelOnlineStatus = "PanelOnlineStatus";

	public const string PanelUIBackground = "PanelUIBackground";

	public const string PanelUIBackgroundCloseBtn = "PanelUIBackgroundCloseBtn";

	public const string PanelValueBeforeAfter_1 = "PanelValueBeforeAfter_1";

	public const string PanelValueBeforeAfter_1_1 = "PanelValueBeforeAfter_1_1";

	public const string PanelValueBeforeAfter_1_2 = "PanelValueBeforeAfter_1_2";

	public const string PanelValueInfo_1 = "PanelValueInfo_1";

	public const string PanelValueInfo_1_1 = "PanelValueInfo_1_1";

	public const string PanelValueInfo_1_2 = "PanelValueInfo_1_2";

	public const string RewardListCell = "RewardListCell";

	public const string TextItemTitle = "TextItemTitle";

	public const string TextListTitle_1 = "TextListTitle_1";

	public const string TextPlayerLevel = "TextPlayerLevel";

	public const string Toggle_1 = "Toggle_1";

	public const string Toggle_2 = "Toggle_2";

	public const string VerticalScrollbar_1 = "VerticalScrollbar_1";

	public const string VerticalScrollbar_2 = "VerticalScrollbar_2";

	public const string UI_CommonCharacterSortFilter = "UI_CommonCharacterSortFilter";

	public const string UI_CommonConsumeMsg = "UI_CommonConsumeMsg";

	public const string UI_CommonFloatInfo = "UI_CommonFloatInfo";

	public const string UI_CommonPlayerInfoMini = "UI_CommonPlayerInfoMini";

	public const string UI_CommonSubMenu = "UI_CommonSubMenu";

	public const string BannerComp = "BannerComp";

	public const string BonusInfoSubMenu = "BonusInfoSubMenu";

	public const string BonusInfoTag = "BonusInfoTag";

	public const string BtnSkip = "BtnSkip";

	public const string StarConditionComp = "StarConditionComp";

	public const string StorageComp00 = "StorageComp00";

	public const string WrapRectComp = "WrapRectComp";

	public const string CrusadeGuildRankingPanelMiniUnit = "CrusadeGuildRankingPanelMiniUnit";

	public const string PanelBossHpBar = "PanelBossHpBar";

	public const string PanelCrusadeBattleSituation = "PanelCrusadeBattleSituation";

	public const string PanelCrusadeGuardianUnit = "PanelCrusadeGuardianUnit";

	public const string PanelCrusadeTimeInfo = "PanelCrusadeTimeInfo";

	public const string UI_CrusadeEntryHint = "UI_CrusadeEntryHint";

	public const string UI_CrusadeGuildRankingPanelMini = "UI_CrusadeGuildRankingPanelMini";

	public const string UI_CrusadeRecord = "UI_CrusadeRecord";

	public const string UI_DeepRecordContinuousMovementLog = "UI_DeepRecordContinuousMovementLog";

	public const string UI_DeepRecordLog = "UI_DeepRecordLog";

	public const string UI_DeepRecordLose = "UI_DeepRecordLose";

	public const string UI_DeepRecordMain = "UI_DeepRecordMain";

	public const string UI_DeepRecordMoveChk = "UI_DeepRecordMoveChk";

	public const string UI_DeepRecordTeamSet = "UI_DeepRecordTeamSet";

	public const string UI_DeepRecordUseSet = "UI_DeepRecordUseSet";

	public const string UI_DeepRecordUseSort = "UI_DeepRecordUseSort";

	public const string UI_DeepRecordWin = "UI_DeepRecordWin";

	public const string UI_CharacterInfoTest = "UI_CharacterInfoTest";

	public const string CardColume = "CardColume";

	public const string CardColumeSmall = "CardColumeSmall";

	public const string CardIcon = "CardIcon";

	public const string CardResetColume = "CardResetColume";

	public const string CharacterColume = "CharacterColume";

	public const string ChipCell = "ChipCell";

	public const string ChipsColume = "ChipsColume";

	public const string ChipVertCell = "ChipVertCell";

	public const string ChipWeaponSelectColume = "ChipWeaponSelectColume";

	public const string RareRoot = "RareRoot";

	public const string RSKillShowColume = "RSKillShowColume";

	public const string UI_ARMORMAIN = "UI_ARMORMAIN";

	public const string UI_BackupSystem = "UI_BackupSystem";

	public const string UI_CardDeployMain = "UI_CardDeployMain";

	public const string UI_CardEquip = "UI_CardEquip";

	public const string UI_CardInfo = "UI_CardInfo";

	public const string UI_CardMain = "UI_CardMain";

	public const string UI_CardMsg = "UI_CardMsg";

	public const string UI_CardReset = "UI_CardReset";

	public const string UI_CardSell = "UI_CardSell";

	public const string UI_CardSellConfirm = "UI_CardSellConfirm";

	public const string UI_CardStorageBuy = "UI_CardStorageBuy";

	public const string UI_CHIPINFO = "UI_CHIPINFO";

	public const string UI_CHIPMAIN = "UI_CHIPMAIN";

	public const string UI_EQUIPMAIN = "UI_EQUIPMAIN";

	public const string UI_QuestTop = "UI_QuestTop";

	public const string UI_Turtorial = "UI_Turtorial";

	public const string UI_WEAPONINFO = "UI_WEAPONINFO";

	public const string UI_WEAPONMAIN = "UI_WEAPONMAIN";

	public const string WeaponCell = "WeaponCell";

	public const string WeaponChipSelectColume = "WeaponChipSelectColume";

	public const string WeaponColume = "WeaponColume";

	public const string WeaponColumeSmall = "WeaponColumeSmall";

	public const string UI_TotalWar = "UI_TotalWar";

	public const string UI_TotalWarRecord = "UI_TotalWarRecord";

	public const string UI_TotalWarReward = "UI_TotalWarReward";

	public const string UI_WORLDBOSSEVENT = "UI_WORLDBOSSEVENT";

	public const string UI_WorldBossRecord = "UI_WorldBossRecord";

	public const string UI_WorldBossReward = "UI_WorldBossReward";

	public const string EddieRewardProgress = "EddieRewardProgress";

	public const string EddieRewardProgressValue = "EddieRewardProgressValue";

	public const string EddieRewardValue = "EddieRewardValue";

	public const string GuildCell = "GuildCell";

	public const string ImageGuildRank = "ImageGuildRank";

	public const string PanelGuildPlayerInfoSimple = "PanelGuildPlayerInfoSimple";

	public const string PanelGuildPrivilege = "PanelGuildPrivilege";

	public const string PanelGuildRankInfo = "PanelGuildRankInfo";

	public const string PanelGuildScoreInfo = "PanelGuildScoreInfo";

	public const string PanelPlayerInfoBeforeAfter = "PanelPlayerInfoBeforeAfter";

	public const string PanelPowerDemand = "PanelPowerDemand";

	public const string PanelUpgradeInfo = "PanelUpgradeInfo";

	public const string FX_GUILD_BUILDING_LEVELUP = "FX_GUILD_BUILDING_LEVELUP";

	public const string FX_GUILD_BUILDING_SHOWUP = "FX_GUILD_BUILDING_SHOWUP";

	public const string FX_guild_levelup = "FX_guild_levelup";

	public const string FX_guild_new_building_00 = "FX_guild_new_building_00";

	public const string FX_GUILD_TELEPORT_IN = "FX_GUILD_TELEPORT_IN";

	public const string FX_GUILD_TELEPORT_OUT = "FX_GUILD_TELEPORT_OUT";

	public const string UI_GuildApplyConfirm = "UI_GuildApplyConfirm";

	public const string UI_GuildApplyConfirmOld = "UI_GuildApplyConfirmOld";

	public const string UI_GuildApplyGuildList = "UI_GuildApplyGuildList";

	public const string UI_GuildBadgeEditor = "UI_GuildBadgeEditor";

	public const string UI_GuildBossMain = "UI_GuildBossMain";

	public const string UI_GuildBossReward = "UI_GuildBossReward";

	public const string UI_GuildChangePresidentList = "UI_GuildChangePresidentList";

	public const string UI_GuildChangePrivilege = "UI_GuildChangePrivilege";

	public const string UI_GuildChangePrivilegeReplaceConfirm = "UI_GuildChangePrivilegeReplaceConfirm";

	public const string UI_GuildCreate = "UI_GuildCreate";

	public const string UI_GuildEddieDonate = "UI_GuildEddieDonate";

	public const string UI_GuildEddieReward = "UI_GuildEddieReward";

	public const string UI_GuildEddieRewardList = "UI_GuildEddieRewardList";

	public const string UI_GuildInviteConfirm = "UI_GuildInviteConfirm";

	public const string UI_GuildInviteGuildList = "UI_GuildInviteGuildList";

	public const string UI_GuildInviteGuildListOld = "UI_GuildInviteGuildListOld";

	public const string UI_GuildInvitePlayerList = "UI_GuildInvitePlayerList";

	public const string UI_GuildInviteSearchList = "UI_GuildInviteSearchList";

	public const string UI_GuildJoin = "UI_GuildJoin";

	public const string UI_GuildJoinOld = "UI_GuildJoinOld";

	public const string UI_GuildList = "UI_GuildList";

	public const string UI_GuildLobby = "UI_GuildLobby";

	public const string UI_GuildMain = "UI_GuildMain";

	public const string UI_GuildMainOld = "UI_GuildMainOld";

	public const string UI_GuildMemberList = "UI_GuildMemberList";

	public const string UI_GuildRankupConfirm = "UI_GuildRankupConfirm";

	public const string UI_GuildSearch = "UI_GuildSearch";

	public const string UI_GuildSearchOld = "UI_GuildSearchOld";

	public const string UI_GuildSubMenu = "UI_GuildSubMenu";

	public const string UI_GuildUpgradeConfirm = "UI_GuildUpgradeConfirm";

	public const string GuildBuffFloatInfoCell = "GuildBuffFloatInfoCell";

	public const string OreCell = "OreCell";

	public const string PowerPillarCell = "PowerPillarCell";

	public const string UI_GuildBuffFloatInfo = "UI_GuildBuffFloatInfo";

	public const string UI_OreLevelUpConfirm = "UI_OreLevelUpConfirm";

	public const string UI_OreSelection = "UI_OreSelection";

	public const string UI_PowerTower = "UI_PowerTower";

	public const string UI_PowerTowerRankupConfirm = "UI_PowerTowerRankupConfirm";

	public const string UI_WantedPlayerInfoMini = "UI_WantedPlayerInfoMini";

	public const string WantedCondition = "WantedCondition";

	public const string WantedMemberSelect = "WantedMemberSelect";

	public const string WantedMemberSelectCell = "WantedMemberSelectCell";

	public const string WantedMemberSelectUnit = "WantedMemberSelectUnit";

	public const string WantedMissionBackground = "WantedMissionBackground";

	public const string WantedMissionDetail = "WantedMissionDetail";

	public const string WantedMissionInfo = "WantedMissionInfo";

	public const string WantedMissionItemTitle = "WantedMissionItemTitle";

	public const string WantedRateInfo = "WantedRateInfo";

	public const string WantedRateInfoUnit = "WantedRateInfoUnit";

	public const string WantedRewardInfo = "WantedRewardInfo";

	public const string WantedRewardInfoUnit = "WantedRewardInfoUnit";

	public const string WantedTargetSelect = "WantedTargetSelect";

	public const string WantedTargetSelectUnit = "WantedTargetSelectUnit";

	public const string WantedTimeHint = "WantedTimeHint";

	public const string UI_WantedFriendSelect = "UI_WantedFriendSelect";

	public const string UI_WantedMain = "UI_WantedMain";

	public const string UI_WantedMemberSelect = "UI_WantedMemberSelect";

	public const string UI_WantedMemberSelectBase = "UI_WantedMemberSelectBase";

	public const string CanvasNoResultMsg = "CanvasNoResultMsg";

	public const string CommonCard_Big = "CommonCard_Big";

	public const string CommonCard_Middle = "CommonCard_Middle";

	public const string CommonCard_Small = "CommonCard_Small";

	public const string CommonGuildBadge = "CommonGuildBadge";

	public const string CommonIconBaseBig = "CommonIconBaseBig";

	public const string CommonIconBaseSmall = "CommonIconBaseSmall";

	public const string CommonIconBaseWanted = "CommonIconBaseWanted";

	public const string CommonSignBase = "CommonSignBase";

	public const string EPRatioDialog = "EPRatioDialog";

	public const string LevelUpEffect = "LevelUpEffect";

	public const string LevelUpWordEffect = "LevelUpWordEffect";

	public const string PlayerIconBase = "PlayerIconBase";

	public const string RankNameStars = "RankNameStars";

	public const string RollingContent = "RollingContent";

	public const string SkillButton = "SkillButton";

	public const string StageIcon = "StageIcon";

	public const string StarUpEffect = "StarUpEffect";

	public const string SweepDialog = "SweepDialog";

	public const string TopRoot = "TopRoot";

	public const string UI_AccountInherit = "UI_AccountInherit";

	public const string UI_AccountLimited = "UI_AccountLimited";

	public const string UI_AgeConfirm = "UI_AgeConfirm";

	public const string UI_Backup_Art = "UI_Backup_Art";

	public const string UI_BattleInvite = "UI_BattleInvite";

	public const string UI_BattleSetting = "UI_BattleSetting";

	public const string UI_BossChallenge = "UI_BossChallenge";

	public const string UI_BossChallengeSweep = "UI_BossChallengeSweep";

	public const string UI_BossIntro = "UI_BossIntro";

	public const string UI_BossStand = "UI_BossStand";

	public const string UI_BoxGachaList = "UI_BoxGachaList";

	public const string UI_ChallengePopup = "UI_ChallengePopup";

	public const string UI_Channel = "UI_Channel";

	public const string UI_CharacterDemo = "UI_CharacterDemo";

	public const string UI_CharacterInfo_Basic = "UI_CharacterInfo_Basic";

	public const string UI_CharacterInfo_Card = "UI_CharacterInfo_Card";

	public const string UI_CharacterInfo_Cell = "UI_CharacterInfo_Cell";

	public const string UI_CharacterInfo_Cell_Small = "UI_CharacterInfo_Cell_Small";

	public const string UI_CharacterInfo_DNA = "UI_CharacterInfo_DNA";

	public const string UI_CharacterInfo_Main = "UI_CharacterInfo_Main";

	public const string UI_CharacterInfo_Portrait = "UI_CharacterInfo_Portrait";

	public const string UI_CharacterInfo_Select = "UI_CharacterInfo_Select";

	public const string UI_CharacterInfo_Skill = "UI_CharacterInfo_Skill";

	public const string UI_CharacterInfo_SkillQUp = "UI_CharacterInfo_SkillQUp";

	public const string UI_CharacterInfo_Skin = "UI_CharacterInfo_Skin";

	public const string UI_CharacterInfo_Sort = "UI_CharacterInfo_Sort";

	public const string UI_CharacterInfo_Upgrade = "UI_CharacterInfo_Upgrade";

	public const string UI_ChargeStamina = "UI_ChargeStamina";

	public const string UI_CommonScrollMsg = "UI_CommonScrollMsg";

	public const string UI_CommonTab = "UI_CommonTab";

	public const string UI_CoopRoomMain = "UI_CoopRoomMain";

	public const string UI_CoopStageSelectUI = "UI_CoopStageSelectUI";

	public const string UI_CreateRoom = "UI_CreateRoom";

	public const string UI_CursorEditor = "UI_CursorEditor";

	public const string UI_Dialog = "UI_Dialog";

	public const string UI_DNABuildList = "UI_DNABuildList";

	public const string UI_DNAConvert = "UI_DNAConvert";

	public const string UI_DNAConvertConfirm = "UI_DNAConvertConfirm";

	public const string UI_DNAExchange = "UI_DNAExchange";

	public const string UI_DNALink = "UI_DNALink";

	public const string UI_DNALinkResult = "UI_DNALinkResult";

	public const string UI_DNAPurchase = "UI_DNAPurchase";

	public const string UI_DNAQuickSkillInfo = "UI_DNAQuickSkillInfo";

	public const string UI_DNAUnlock = "UI_DNAUnlock";

	public const string UI_EquipGetPopup = "UI_EquipGetPopup";

	public const string UI_EquipmentDismantle = "UI_EquipmentDismantle";

	public const string UI_EquipmentDismantleConfirm = "UI_EquipmentDismantleConfirm";

	public const string UI_EventReward = "UI_EventReward";

	public const string UI_EventStage = "UI_EventStage";

	public const string UI_FinalStrikeMain = "UI_FinalStrikeMain";

	public const string UI_FriendBattle = "UI_FriendBattle";

	public const string UI_FriendConfirm = "UI_FriendConfirm";

	public const string UI_FriendInvite = "UI_FriendInvite";

	public const string UI_FriendInviteList = "UI_FriendInviteList";

	public const string UI_FriendMain = "UI_FriendMain";

	public const string UI_FriendPVPCreateRoom = "UI_FriendPVPCreateRoom";

	public const string UI_FriendPVPRoomMain = "UI_FriendPVPRoomMain";

	public const string UI_FriendPVPSelectRoom = "UI_FriendPVPSelectRoom";

	public const string UI_FriendSearchList = "UI_FriendSearchList";

	public const string UI_Gacha = "UI_Gacha";

	public const string UI_GachaCeiling = "UI_GachaCeiling";

	public const string UI_GachaCeilingExchange = "UI_GachaCeilingExchange";

	public const string UI_GachaConfirm = "UI_GachaConfirm";

	public const string UI_GachaResult = "UI_GachaResult";

	public const string UI_GachaSkip = "UI_GachaSkip";

	public const string UI_GachaTap = "UI_GachaTap";

	public const string UI_GameInputEditor = "UI_GameInputEditor";

	public const string UI_GamePause = "UI_GamePause";

	public const string UI_Guide = "UI_Guide";

	public const string UI_Hometop = "UI_Hometop";

	public const string UI_HometopOld = "UI_HometopOld";

	public const string UI_Illustration = "UI_Illustration";

	public const string UI_IllustrationTarget = "UI_IllustrationTarget";

	public const string UI_InputPassword = "UI_InputPassword";

	public const string UI_InputPlayerName = "UI_InputPlayerName";

	public const string UI_InputText = "UI_InputText";

	public const string UI_ItemBox = "UI_ItemBox";

	public const string UI_ItemBoxEquipCompose = "UI_ItemBoxEquipCompose";

	public const string UI_ItemBoxEquipInfo = "UI_ItemBoxEquipInfo";

	public const string UI_ItemBoxEquipUpgrade = "UI_ItemBoxEquipUpgrade";

	public const string UI_ItemBoxEquipUpgrade2 = "UI_ItemBoxEquipUpgrade2";

	public const string UI_ItemHowToGet = "UI_ItemHowToGet";

	public const string UI_ItemInfo = "UI_ItemInfo";

	public const string UI_ItemSell = "UI_ItemSell";

	public const string UI_ItemUse = "UI_ItemUse";

	public const string UI_Locate = "UI_Locate";

	public const string UI_LoginBonusLoop = "UI_LoginBonusLoop";

	public const string UI_LoginBonusSpecial = "UI_LoginBonusSpecial";

	public const string UI_lvup = "UI_lvup";

	public const string UI_MailDetail = "UI_MailDetail";

	public const string UI_MailSelect = "UI_MailSelect";

	public const string UI_MenuGoPlay = "UI_MenuGoPlay";

	public const string UI_Minigame01 = "UI_Minigame01";

	public const string UI_Mission = "UI_Mission";

	public const string UI_MultiChannel = "UI_MultiChannel";

	public const string UI_NewRewardPopup = "UI_NewRewardPopup";

	public const string UI_PlayerCustomize = "UI_PlayerCustomize";

	public const string UI_PlayerEditor = "UI_PlayerEditor";

	public const string UI_PlayerInfoMain = "UI_PlayerInfoMain";

	public const string UI_PopupDesc = "UI_PopupDesc";

	public const string UI_powerup = "UI_powerup";

	public const string UI_PrizeReward = "UI_PrizeReward";

	public const string UI_PrizeTop = "UI_PrizeTop";

	public const string UI_PVP_rankUP = "UI_PVP_rankUP";

	public const string UI_PVP_rankUP_star = "UI_PVP_rankUP_star";

	public const string UI_PvpBar = "UI_PvpBar";

	public const string UI_PvpEnd = "UI_PvpEnd";

	public const string UI_PvpLoading = "UI_PvpLoading";

	public const string UI_PvpMatch = "UI_PvpMatch";

	public const string UI_PvpReport = "UI_PvpReport";

	public const string UI_PvpReward = "UI_PvpReward";

	public const string UI_PvpRoomSelect = "UI_PvpRoomSelect";

	public const string UI_Qualifing = "UI_Qualifing";

	public const string UI_RankingMain = "UI_RankingMain";

	public const string UI_RankPanelMini = "UI_RankPanelMini";

	public const string UI_Ready = "UI_Ready";

	public const string UI_Research = "UI_Research";

	public const string UI_RewardPopup = "UI_RewardPopup";

	public const string UI_ROGLogin = "UI_ROGLogin";

	public const string UI_RoomSelect = "UI_RoomSelect";

	public const string UI_RuleBonus = "UI_RuleBonus";

	public const string UI_ServerRangeTab = "UI_ServerRangeTab";

	public const string UI_ServerSelect = "UI_ServerSelect";

	public const string UI_ServerSelect_2 = "UI_ServerSelect_2";

	public const string UI_Setting = "UI_Setting";

	public const string UI_ShinyEffect = "UI_ShinyEffect";

	public const string UI_ShopBuy = "UI_ShopBuy";

	public const string UI_ShopBuyIAP = "UI_ShopBuyIAP";

	public const string UI_ShopTop = "UI_ShopTop";

	public const string UI_SNS = "UI_SNS";

	public const string UI_SNSIcons = "UI_SNSIcons";

	public const string UI_StageLoading = "UI_StageLoading";

	public const string UI_StageNoise = "UI_StageNoise";

	public const string UI_StarConditionPopup = "UI_StarConditionPopup";

	public const string UI_STORYMAIN = "UI_STORYMAIN";

	public const string UI_StoryStageSelect = "UI_StoryStageSelect";

	public const string UI_SubMenu = "UI_SubMenu";

	public const string UI_TargetMark = "UI_TargetMark";

	public const string UI_TermsOfUse = "UI_TermsOfUse";

	public const string UI_TermsOfUse_JP = "UI_TermsOfUse_JP";

	public const string UI_Tip = "UI_Tip";

	public const string UI_TipResult = "UI_TipResult";

	public const string UI_Title_3 = "UI_Title_3";

	public const string UI_Title_3_JP = "UI_Title_3_JP";

	public const string UI_TitlePolicy = "UI_TitlePolicy";

	public const string UI_TitleSub = "UI_TitleSub";

	public const string UI_TopResident = "UI_TopResident";

	public const string UI_Tower_Art = "UI_Tower_Art";

	public const string UI_VirtualPadEditor = "UI_VirtualPadEditor";

	public const string Upgrade3DEffect = "Upgrade3DEffect";

	public const string UpgradeEffect = "UpgradeEffect";

	public const string UI_CommonMsg = "UI_CommonMsg";

	public const string UI_CommonResLoadingBar = "UI_CommonResLoadingBar";

	public const string UI_EmptyBlock = "UI_EmptyBlock";

	public const string UI_FullLoading = "UI_FullLoading";

	public const string UI_Language = "UI_Language";

	public const string UI_Msg = "UI_Msg";

	public const string UI_TipLoading = "UI_TipLoading";

	public const string UI_WebView = "UI_WebView";

	public const string UI_WebView_Notice = "UI_WebView_Notice";

	public const string UI_WorldServerSelect = "UI_WorldServerSelect";
}
