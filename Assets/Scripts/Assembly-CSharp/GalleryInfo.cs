using System.Collections.Generic;

public class GalleryInfo
{
	public List<NetGalleryInfo> GalleryList = new List<NetGalleryInfo>();

	public List<NetGalleryExpInfo> GalleryExpList = new List<NetGalleryExpInfo>();

	public List<NetGalleryMainIdInfo> GalleryCardList = new List<NetGalleryMainIdInfo>();
}
