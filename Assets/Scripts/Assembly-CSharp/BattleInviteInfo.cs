public class BattleInviteInfo
{
	public string InviterName;

	public string Host;

	public int Port;

	public string RoomId;

	public int StageId;

	public int Capacity;
}
