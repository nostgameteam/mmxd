public enum CharaSE_FORTEEXE
{
	NONE = 0,
	CRI_CHARASE_FORTEEXE_CUENUM = 10,
	CRI_CHARASE_FORTEEXE_FE_JUMP = 1,
	CRI_CHARASE_FORTEEXE_FE_JUMP_HIGH = 2,
	CRI_CHARASE_FORTEEXE_FE_CHAKUCHI = 3,
	CRI_CHARASE_FORTEEXE_FE_DASH = 4,
	CRI_CHARASE_FORTEEXE_FE_DASHEND = 5,
	CRI_CHARASE_FORTEEXE_FE_KABEPETA = 6,
	CRI_CHARASE_FORTEEXE_FE_KABEKERI = 7,
	CRI_CHARASE_FORTEEXE_FE_TELEPORT_IN = 8,
	CRI_CHARASE_FORTEEXE_FE_TELEPORT_OUT = 9,
	CRI_CHARASE_FORTEEXE_FE_FOOT = 10
}
