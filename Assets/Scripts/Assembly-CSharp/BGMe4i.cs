public static class BGMe4i
{
	public const int iCRI_BGME4_CUENUM = 3;

	public const int iCRI_BGME4_BGM_ST_E4 = 3;

	public const int iCRI_BGME4_BGM_SYS_E4H = 7;

	public const int iCRI_BGME4_BGM_SYS_E4H_2 = 8;

	public const BGMe4 CRI_BGME4_CUENUM = BGMe4.CRI_BGME4_CUENUM;

	public const BGMe4 CRI_BGME4_BGM_ST_E4 = BGMe4.CRI_BGME4_CUENUM;

	public const BGMe4 CRI_BGME4_BGM_SYS_E4H = BGMe4.CRI_BGME4_BGM_SYS_E4H;

	public const BGMe4 CRI_BGME4_BGM_SYS_E4H_2 = BGMe4.CRI_BGME4_BGM_SYS_E4H_2;
}
