using UnityEngine;

public class CoroutineDefine
{
	public static readonly WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame();

	public static WaitForSeconds _0_3sec = new WaitForSeconds(0.3f);

	public static WaitForSeconds _1sec = new WaitForSeconds(1f);
}
