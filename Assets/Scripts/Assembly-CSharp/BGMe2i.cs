public static class BGMe2i
{
	public const int iCRI_BGME2_CUENUM = 2;

	public const int iCRI_BGME2_BGM_SYS_E2 = 1;

	public const int iCRI_BGME2_BGM_SYS_E2H = 7;

	public const BGMe2 CRI_BGME2_CUENUM = BGMe2.CRI_BGME2_CUENUM;

	public const BGMe2 CRI_BGME2_BGM_SYS_E2 = BGMe2.CRI_BGME2_BGM_SYS_E2;

	public const BGMe2 CRI_BGME2_BGM_SYS_E2H = BGMe2.CRI_BGME2_BGM_SYS_E2H;
}
