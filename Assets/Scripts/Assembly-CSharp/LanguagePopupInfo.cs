using System;
using enums;

[Serializable]
public class LanguagePopupInfo
{
	public string Language;

	public string LanguageTitle;

	public string BtnOK;

	public bool IsOpen;

	public Language languageEnum;

	public string fontName;

	public bool loadFromRes;
}
