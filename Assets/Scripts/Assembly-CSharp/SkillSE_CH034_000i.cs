public static class SkillSE_CH034_000i
{
	public const int iCRI_SKILLSE_CH034_000_CUENUM = 13;

	public const int iCRI_SKILLSE_CH034_000_CH034_SHOT_01 = 1;

	public const int iCRI_SKILLSE_CH034_000_CH034_SHOT_02 = 2;

	public const int iCRI_SKILLSE_CH034_000_CH034_SHOT_03 = 3;

	public const int iCRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_LP = 4;

	public const int iCRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_STOP = 5;

	public const int iCRI_SKILLSE_CH034_000_CH034_CUT_01 = 6;

	public const int iCRI_SKILLSE_CH034_000_CH034_CUT_02 = 7;

	public const int iCRI_SKILLSE_CH034_000_CH034_CUT_03 = 8;

	public const int iCRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_LP = 9;

	public const int iCRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_STOP = 10;

	public const int iCRI_SKILLSE_CH034_000_CH034_KUENBU = 11;

	public const int iCRI_SKILLSE_CH034_000_CH034_CHARA01 = 12;

	public const int iCRI_SKILLSE_CH034_000_CH034_START01 = 13;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CUENUM = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CUENUM;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_SHOT_01 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_SHOT_01;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_SHOT_02 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_SHOT_02;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_SHOT_03 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_SHOT_03;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_LP = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_LP;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_STOP = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CHARGE_SHOT_STOP;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CUT_01 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CUT_01;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CUT_02 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CUT_02;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CUT_03 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CUT_03;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_LP = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_LP;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_STOP = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CHARGE_CUT_STOP;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_KUENBU = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_KUENBU;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_CHARA01 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CH034_CHARA01;

	public const SkillSE_CH034_000 CRI_SKILLSE_CH034_000_CH034_START01 = SkillSE_CH034_000.CRI_SKILLSE_CH034_000_CUENUM;
}
