public enum BattleSE02
{
	NONE = 0,
	CRI_BATTLESE02_CUENUM = 88,
	CRI_BATTLESE02_BT_WARP = 31,
	CRI_BATTLESE02_BT_RUSH01 = 33,
	CRI_BATTLESE02_BT_RUSH02 = 34,
	CRI_BATTLESE02_BT_BEAT01 = 36,
	CRI_BATTLESE02_BT_BEAT02 = 37,
	CRI_BATTLESE02_BT_BEAT03 = 38,
	CRI_BATTLESE02_BT_EDY01 = 40,
	CRI_BATTLESE02_BT_EDY02 = 41,
	CRI_BATTLESE02_BT_RUSH04_STOP = 45,
	CRI_BATTLESE02_BT_RUSH03 = 35,
	CRI_BATTLESE02_BT_RUSH04_LG = 44,
	CRI_BATTLESE02_BT_RUSH04_LP = 46,
	CRI_BATTLESE02_BT_RUSH05 = 47,
	CRI_BATTLESE02_BT_ELEVATOR02_LP = 48,
	CRI_BATTLESE02_BT_ELEVATOR02_STOP = 49,
	CRI_BATTLESE02_BT_ZIWARE01 = 50,
	CRI_BATTLESE02_BT_BLOCK_APPEAR = 52,
	CRI_BATTLESE02_BT_BLOCK_VANISH = 53,
	CRI_BATTLESE02_BT_BUCKET = 54,
	CRI_BATTLESE02_BT_SURF_LP = 55,
	CRI_BATTLESE02_BT_SURF_STOP = 56,
	CRI_BATTLESE02_BT_BL_CHARGE_LP = 1,
	CRI_BATTLESE02_BT_RM_CHARGE_LP = 2,
	CRI_BATTLESE02_BT_RM2_CHARGE_LP = 3,
	CRI_BATTLESE02_BT_CH033_CHARGE_LP = 4,
	CRI_BATTLESE02_BT_CH043_CHARGE_LP = 57,
	CRI_BATTLESE02_BT_CH035_CHARGE_LP = 58,
	CRI_BATTLESE02_BT_CH049_CHARGE_LP = 59,
	CRI_BATTLESE02_BT_CH034_CHARGE_SHOT_LP = 60,
	CRI_BATTLESE02_BT_CH034_CHARGE_CUT_LP = 61,
	CRI_BATTLESE02_BT_BL_CHARGE_STOP = 62,
	CRI_BATTLESE02_BT_RM_CHARGE_STOP = 63,
	CRI_BATTLESE02_BT_RM2_CHARGE_STOP = 64,
	CRI_BATTLESE02_BT_CH033_CHARGE_STOP = 65,
	CRI_BATTLESE02_BT_CH043_CHARGE_STOP = 66,
	CRI_BATTLESE02_BT_CH035_CHARGE_STOP = 67,
	CRI_BATTLESE02_BT_CH049_CHARGE_STOP = 68,
	CRI_BATTLESE02_BT_CH034_CHARGE_SHOT_STOP = 69,
	CRI_BATTLESE02_BT_CH034_CHARGE_CUT_STOP = 70,
	CRI_BATTLESE02_BT_CAN = 71,
	CRI_BATTLESE02_BT_CAN02 = 78,
	CRI_BATTLESE02_BT_CHAIN01_LP = 72,
	CRI_BATTLESE02_BT_CHAIN01_STOP = 73,
	CRI_BATTLESE02_BT_QUAKE01 = 74,
	CRI_BATTLESE02_BT_CRATE01 = 75,
	CRI_BATTLESE02_BT_BIT01 = 43,
	CRI_BATTLESE02_BT_BIT02 = 77,
	CRI_BATTLESE02_BT_RE_CHARGE02_LP = 79,
	CRI_BATTLESE02_BT_RE_CHARGEMAX03 = 103,
	CRI_BATTLESE02_BT_RE_CHARGE03_STOP = 102,
	CRI_BATTLESE02_BT_RE_CHARGE03_LP = 101,
	CRI_BATTLESE02_BT_FO_CHARGE_STOP = 97,
	CRI_BATTLESE02_BT_FO_CHARGE_LP = 96,
	CRI_BATTLESE02_BT_RE_CHARGE02_STOP = 80,
	CRI_BATTLESE02_BT_KAKERIMUSHI01 = 25,
	CRI_BATTLESE02_BT_KAKERIMUSHI02 = 26,
	CRI_BATTLESE02_BT_WARP02 = 32,
	CRI_BATTLESE02_BT_XK_CHARGE_LP = 83,
	CRI_BATTLESE02_BT_XK_CHARGE02_LP = 84,
	CRI_BATTLESE02_BT_XK_CHARGE_STOP = 85,
	CRI_BATTLESE02_BT_XD_CHARGE_STOP = 87,
	CRI_BATTLESE02_BT_XD_CHARGE_LP = 86,
	CRI_BATTLESE02_BT_RS_CHARGE_LP = 88,
	CRI_BATTLESE02_BT_RS_CHARGE_STOP = 89,
	CRI_BATTLESE02_BT_RE_CHARGE_LP = 90,
	CRI_BATTLESE02_BT_RE_CHARGE_STOP = 91,
	CRI_BATTLESE02_BT_ELEVATOR03_LP = 29,
	CRI_BATTLESE02_BT_ELEVATOR03_STOP = 30,
	CRI_BATTLESE02_BT_X4_CHARGE_LP = 92,
	CRI_BATTLESE02_BT_X4_CHARGE_STOP = 93,
	CRI_BATTLESE02_BT_XG_CHARGE_STOP = 95,
	CRI_BATTLESE02_BT_XG_CHARGE_LP = 94,
	CRI_BATTLESE02_BT_QUICKSAND_LP = 27,
	CRI_BATTLESE02_BT_QUICKSAND_STOP = 28,
	CRI_BATTLESE02_BT_RE_CHARGEMAX02 = 81,
	CRI_BATTLESE02_BT_AI2_CHARGE01_LP = 98,
	CRI_BATTLESE02_BT_AI2_CHARGE01_STOP = 99,
	CRI_BATTLESE02_BT_AI2_CHARGE02 = 100,
	CRI_BATTLESE02_BT_REXRIDE01 = 20,
	CRI_BATTLESE02_BT_REXRIDE02 = 21,
	CRI_BATTLESE02_BT_REXRIDE03 = 22,
	CRI_BATTLESE02_BT_REXRIDE04 = 23,
	CRI_BATTLESE02_BT_REXRIDE00 = 19,
	CRI_BATTLESE02_BT_REXRIDE05 = 24,
	CRI_BATTLESE02_BT_XD2_CHARGE_LP = 104,
	CRI_BATTLESE02_BT_XD2_CHARGE_STOP = 105,
	CRI_BATTLESE02_BT_XD2_CHARGELVUP = 106,
	CRI_BATTLESE02_BT_XD2_CHARGEMAX = 107
}
