public static class VOICE_ERATO2i
{
	public const int iCRI_VOICE_ERATO2_CUENUM = 17;

	public const int iCRI_VOICE_ERATO2_V_ER2_HURT01 = 1;

	public const int iCRI_VOICE_ERATO2_V_ER2_ATTACK01 = 25;

	public const int iCRI_VOICE_ERATO2_V_ER2_SKILL01 = 8;

	public const int iCRI_VOICE_ERATO2_V_ER2_SKILL02 = 9;

	public const int iCRI_VOICE_ERATO2_V_ER2_PINCH01 = 20;

	public const int iCRI_VOICE_ERATO2_V_ER2_PINCH02 = 21;

	public const int iCRI_VOICE_ERATO2_V_ER2_PINCH03 = 22;

	public const int iCRI_VOICE_ERATO2_V_ER2_LOSE01 = 5;

	public const int iCRI_VOICE_ERATO2_V_ER2_JUMP01 = 12;

	public const int iCRI_VOICE_ERATO2_V_ER2_UNIQUE01 = 6;

	public const int iCRI_VOICE_ERATO2_V_ER2_WIN01 = 3;

	public const int iCRI_VOICE_ERATO2_V_ER2_SABER01 = 14;

	public const int iCRI_VOICE_ERATO2_V_ER2_SABER03 = 16;

	public const int iCRI_VOICE_ERATO2_V_ER2_SABER05 = 18;

	public const int iCRI_VOICE_ERATO2_V_ER2_START01 = 2;

	public const int iCRI_VOICE_ERATO2_V_ER2_CHARA01 = 40;

	public const int iCRI_VOICE_ERATO2_V_ER2_SKILL03 = 10;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_CUENUM = VOICE_ERATO2.CRI_VOICE_ERATO2_CUENUM;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_HURT01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_HURT01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_ATTACK01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_ATTACK01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SKILL01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SKILL01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SKILL02 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SKILL02;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_PINCH01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_PINCH01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_PINCH02 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_PINCH02;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_PINCH03 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_PINCH03;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_LOSE01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_LOSE01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_JUMP01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_JUMP01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_UNIQUE01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_UNIQUE01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_WIN01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_WIN01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SABER01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SABER01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SABER03 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SABER03;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SABER05 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SABER05;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_START01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_START01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_CHARA01 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_CHARA01;

	public const VOICE_ERATO2 CRI_VOICE_ERATO2_V_ER2_SKILL03 = VOICE_ERATO2.CRI_VOICE_ERATO2_V_ER2_SKILL03;
}
