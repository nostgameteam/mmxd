public enum CharaSE_PALETTE
{
	NONE = 0,
	CRI_CHARASE_PALETTE_CUENUM = 10,
	CRI_CHARASE_PALETTE_PL_JUMP = 1,
	CRI_CHARASE_PALETTE_PL_JUMP_HIGH = 2,
	CRI_CHARASE_PALETTE_PL_CHAKUCHI = 3,
	CRI_CHARASE_PALETTE_PL_DASH = 4,
	CRI_CHARASE_PALETTE_PL_DASHEND = 5,
	CRI_CHARASE_PALETTE_PL_KABEPETA = 6,
	CRI_CHARASE_PALETTE_PL_KABEKERI = 7,
	CRI_CHARASE_PALETTE_PL_TELEPORT_IN = 8,
	CRI_CHARASE_PALETTE_PL_TELEPORT_OUT = 9,
	CRI_CHARASE_PALETTE_PL_FOOT = 10
}
