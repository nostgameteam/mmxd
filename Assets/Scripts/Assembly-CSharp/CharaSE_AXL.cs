public enum CharaSE_AXL
{
	NONE = 0,
	CRI_CHARASE_AXL_CUENUM = 10,
	CRI_CHARASE_AXL_A_JUMP = 1,
	CRI_CHARASE_AXL_A_JUMP_HIGH = 2,
	CRI_CHARASE_AXL_A_CHAKUCHI = 3,
	CRI_CHARASE_AXL_A_DASH = 4,
	CRI_CHARASE_AXL_A_DASHEND = 5,
	CRI_CHARASE_AXL_A_KABEPETA = 6,
	CRI_CHARASE_AXL_A_KABEKERI = 7,
	CRI_CHARASE_AXL_A_TELEPORT_IN = 8,
	CRI_CHARASE_AXL_A_TELEPORT_OUT = 9,
	CRI_CHARASE_AXL_A_FOOT = 10
}
