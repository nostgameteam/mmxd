public static class SkillSE_Xi
{
	public const int iCRI_SKILLSE_X_CUENUM = 12;

	public const int iCRI_SKILLSE_X_X_CHARGESHOT_S = 1;

	public const int iCRI_SKILLSE_X_X_CHARGESHOT_M = 3;

	public const int iCRI_SKILLSE_X_X_CHARGESHOT_L = 5;

	public const int iCRI_SKILLSE_X_X_CHARGEMAX = 6;

	public const int iCRI_SKILLSE_X_X_CHARGE_LP = 7;

	public const int iCRI_SKILLSE_X_X_CHARGE_STOP = 8;

	public const int iCRI_SKILLSE_X_X_ROCKET = 9;

	public const int iCRI_SKILLSE_X_X_RISING = 12;

	public const int iCRI_SKILLSE_X_X_HADO = 11;

	public const int iCRI_SKILLSE_X_X_BLAZE_LP = 13;

	public const int iCRI_SKILLSE_X_X_BLAZE_STOP = 14;

	public const int iCRI_SKILLSE_X_X_CHARA01 = 40;

	public const SkillSE_X CRI_SKILLSE_X_CUENUM = SkillSE_X.CRI_SKILLSE_X_CUENUM;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGESHOT_S = SkillSE_X.CRI_SKILLSE_X_X_CHARGESHOT_S;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGESHOT_M = SkillSE_X.CRI_SKILLSE_X_X_CHARGESHOT_M;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGESHOT_L = SkillSE_X.CRI_SKILLSE_X_X_CHARGESHOT_L;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGEMAX = SkillSE_X.CRI_SKILLSE_X_X_CHARGEMAX;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGE_LP = SkillSE_X.CRI_SKILLSE_X_X_CHARGE_LP;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARGE_STOP = SkillSE_X.CRI_SKILLSE_X_X_CHARGE_STOP;

	public const SkillSE_X CRI_SKILLSE_X_X_ROCKET = SkillSE_X.CRI_SKILLSE_X_X_ROCKET;

	public const SkillSE_X CRI_SKILLSE_X_X_RISING = SkillSE_X.CRI_SKILLSE_X_CUENUM;

	public const SkillSE_X CRI_SKILLSE_X_X_HADO = SkillSE_X.CRI_SKILLSE_X_X_HADO;

	public const SkillSE_X CRI_SKILLSE_X_X_BLAZE_LP = SkillSE_X.CRI_SKILLSE_X_X_BLAZE_LP;

	public const SkillSE_X CRI_SKILLSE_X_X_BLAZE_STOP = SkillSE_X.CRI_SKILLSE_X_X_BLAZE_STOP;

	public const SkillSE_X CRI_SKILLSE_X_X_CHARA01 = SkillSE_X.CRI_SKILLSE_X_X_CHARA01;
}
