public static class CharaSE_CINNAMONi
{
	public const int iCRI_CHARASE_CINNAMON_CUENUM = 10;

	public const int iCRI_CHARASE_CINNAMON_SM_JUMP = 1;

	public const int iCRI_CHARASE_CINNAMON_SM_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_CINNAMON_SM_CHAKUCHI = 3;

	public const int iCRI_CHARASE_CINNAMON_SM_DASH = 4;

	public const int iCRI_CHARASE_CINNAMON_SM_DASHEND = 5;

	public const int iCRI_CHARASE_CINNAMON_SM_KABEPETA = 6;

	public const int iCRI_CHARASE_CINNAMON_SM_KABEKERI = 7;

	public const int iCRI_CHARASE_CINNAMON_SM_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_CINNAMON_SM_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_CINNAMON_SM_FOOT = 10;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_CUENUM = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_CUENUM;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_JUMP = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_JUMP;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_JUMP_HIGH = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_JUMP_HIGH;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_CHAKUCHI = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_CHAKUCHI;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_DASH = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_DASH;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_DASHEND = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_DASHEND;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_KABEPETA = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_KABEPETA;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_KABEKERI = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_KABEKERI;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_TELEPORT_IN = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_TELEPORT_IN;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_TELEPORT_OUT = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_SM_TELEPORT_OUT;

	public const CharaSE_CINNAMON CRI_CHARASE_CINNAMON_SM_FOOT = CharaSE_CINNAMON.CRI_CHARASE_CINNAMON_CUENUM;
}
