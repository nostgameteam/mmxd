public static class CharaSE_XShadowi
{
	public const int iCRI_CHARASE_XSHADOW_CUENUM = 10;

	public const int iCRI_CHARASE_XSHADOW_XS_JUMP = 1;

	public const int iCRI_CHARASE_XSHADOW_XS_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_XSHADOW_XS_CHAKUCHI = 3;

	public const int iCRI_CHARASE_XSHADOW_XS_DASH = 4;

	public const int iCRI_CHARASE_XSHADOW_XS_DASHEND = 5;

	public const int iCRI_CHARASE_XSHADOW_XS_KABEPETA = 6;

	public const int iCRI_CHARASE_XSHADOW_XS_KABEKERI = 7;

	public const int iCRI_CHARASE_XSHADOW_XS_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_XSHADOW_XS_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_XSHADOW_XS_FOOT = 10;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_CUENUM = CharaSE_XShadow.CRI_CHARASE_XSHADOW_CUENUM;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_JUMP = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_JUMP;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_JUMP_HIGH = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_JUMP_HIGH;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_CHAKUCHI = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_CHAKUCHI;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_DASH = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_DASH;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_DASHEND = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_DASHEND;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_KABEPETA = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_KABEPETA;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_KABEKERI = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_KABEKERI;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_TELEPORT_IN = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_TELEPORT_IN;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_TELEPORT_OUT = CharaSE_XShadow.CRI_CHARASE_XSHADOW_XS_TELEPORT_OUT;

	public const CharaSE_XShadow CRI_CHARASE_XSHADOW_XS_FOOT = CharaSE_XShadow.CRI_CHARASE_XSHADOW_CUENUM;
}
