public class PetSummoner : IPetSummoner
{
	public int PetID { get; set; }

	public long PetTime { get; set; }

	public int PetCount { get; set; }
}
