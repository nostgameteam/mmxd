public interface IPetSummoner
{
	int PetID { get; set; }

	long PetTime { get; set; }

	int PetCount { get; set; }
}
