public class SocketRankingInfo
{
	public int m_Index;

	public int m_Rank;

	public string m_PlayerId;

	public string m_PlayerName;

	public int m_Score;

	public string m_PlayerIcon;

	public string m_PlayerVIP;

	public int m_PlayerModelID;

	public int m_BestWeaponModelID;

	public int m_MainWeaponModelID;

	public int m_PlayerModelSkin;

	public int m_BestWeaponModelSkin;

	public int m_MainWeaponModelSkin;

	public bool m_bConvertScoreToTime;
}
