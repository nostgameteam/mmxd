public static class CharaSE_VIAi
{
	public const int iCRI_CHARASE_VIA_CUENUM = 10;

	public const int iCRI_CHARASE_VIA_VI_JUMP = 1;

	public const int iCRI_CHARASE_VIA_VI_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_VIA_VI_CHAKUCHI = 3;

	public const int iCRI_CHARASE_VIA_VI_DASH = 4;

	public const int iCRI_CHARASE_VIA_VI_DASHEND = 5;

	public const int iCRI_CHARASE_VIA_VI_KABEPETA = 6;

	public const int iCRI_CHARASE_VIA_VI_KABEKERI = 7;

	public const int iCRI_CHARASE_VIA_VI_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_VIA_VI_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_VIA_VI_FOOT = 10;

	public const CharaSE_VIA CRI_CHARASE_VIA_CUENUM = CharaSE_VIA.CRI_CHARASE_VIA_CUENUM;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_JUMP = CharaSE_VIA.CRI_CHARASE_VIA_VI_JUMP;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_JUMP_HIGH = CharaSE_VIA.CRI_CHARASE_VIA_VI_JUMP_HIGH;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_CHAKUCHI = CharaSE_VIA.CRI_CHARASE_VIA_VI_CHAKUCHI;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_DASH = CharaSE_VIA.CRI_CHARASE_VIA_VI_DASH;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_DASHEND = CharaSE_VIA.CRI_CHARASE_VIA_VI_DASHEND;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_KABEPETA = CharaSE_VIA.CRI_CHARASE_VIA_VI_KABEPETA;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_KABEKERI = CharaSE_VIA.CRI_CHARASE_VIA_VI_KABEKERI;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_TELEPORT_IN = CharaSE_VIA.CRI_CHARASE_VIA_VI_TELEPORT_IN;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_TELEPORT_OUT = CharaSE_VIA.CRI_CHARASE_VIA_VI_TELEPORT_OUT;

	public const CharaSE_VIA CRI_CHARASE_VIA_VI_FOOT = CharaSE_VIA.CRI_CHARASE_VIA_CUENUM;
}
