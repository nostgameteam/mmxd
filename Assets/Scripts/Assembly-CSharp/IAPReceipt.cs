using enums;

public class IAPReceipt
{
	public string TransactionID;

	public string Payload;

	public IAPStoreType StoreType;

	public int ShopItemID;

	public string ProductID;
}
