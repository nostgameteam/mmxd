public static class CharaSE_UltimateX2i
{
	public const int iCRI_CHARASE_ULTIMATEX2_CUENUM = 10;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_JUMP = 1;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_CHAKUCHI = 3;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_DASH = 4;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_DASHEND = 5;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_KABEPETA = 6;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_KABEKERI = 7;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_ULTIMATEX2_XU2_FOOT = 10;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_CUENUM = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_CUENUM;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_JUMP = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_JUMP;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_JUMP_HIGH = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_JUMP_HIGH;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_CHAKUCHI = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_CHAKUCHI;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_DASH = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_DASH;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_DASHEND = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_DASHEND;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_KABEPETA = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_KABEPETA;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_KABEKERI = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_KABEKERI;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_IN = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_IN;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_OUT = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_XU2_TELEPORT_OUT;

	public const CharaSE_UltimateX2 CRI_CHARASE_ULTIMATEX2_XU2_FOOT = CharaSE_UltimateX2.CRI_CHARASE_ULTIMATEX2_CUENUM;
}
