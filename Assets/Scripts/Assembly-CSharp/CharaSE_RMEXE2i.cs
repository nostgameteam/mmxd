public static class CharaSE_RMEXE2i
{
	public const int iCRI_CHARASE_RMEXE2_CUENUM = 10;

	public const int iCRI_CHARASE_RMEXE2_RE2_JUMP = 1;

	public const int iCRI_CHARASE_RMEXE2_RE2_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_RMEXE2_RE2_CHAKUCHI = 3;

	public const int iCRI_CHARASE_RMEXE2_RE2_DASH = 4;

	public const int iCRI_CHARASE_RMEXE2_RE2_DASHEND = 5;

	public const int iCRI_CHARASE_RMEXE2_RE2_KABEPETA = 6;

	public const int iCRI_CHARASE_RMEXE2_RE2_KABEKERI = 7;

	public const int iCRI_CHARASE_RMEXE2_RE2_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_RMEXE2_RE2_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_RMEXE2_RE2_FOOT = 10;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_CUENUM = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_CUENUM;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_JUMP = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_JUMP;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_JUMP_HIGH = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_JUMP_HIGH;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_CHAKUCHI = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_CHAKUCHI;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_DASH = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_DASH;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_DASHEND = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_DASHEND;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_KABEPETA = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_KABEPETA;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_KABEKERI = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_KABEKERI;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_TELEPORT_IN = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_TELEPORT_IN;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_TELEPORT_OUT = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_RE2_TELEPORT_OUT;

	public const CharaSE_RMEXE2 CRI_CHARASE_RMEXE2_RE2_FOOT = CharaSE_RMEXE2.CRI_CHARASE_RMEXE2_CUENUM;
}
