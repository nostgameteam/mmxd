public enum CharaSE_RYU
{
	NONE = 0,
	CRI_CHARASE_RYU_CUENUM = 10,
	CRI_CHARASE_RYU_RY_JUMP = 1,
	CRI_CHARASE_RYU_RY_JUMP_HIGH = 2,
	CRI_CHARASE_RYU_RY_CHAKUCHI = 3,
	CRI_CHARASE_RYU_RY_DASH = 4,
	CRI_CHARASE_RYU_RY_DASHEND = 5,
	CRI_CHARASE_RYU_RY_KABEPETA = 6,
	CRI_CHARASE_RYU_RY_KABEKERI = 7,
	CRI_CHARASE_RYU_RY_TELEPORT_IN = 8,
	CRI_CHARASE_RYU_RY_TELEPORT_OUT = 9,
	CRI_CHARASE_RYU_RY_FOOT = 10
}
