public static class BGMe1i
{
	public const int iCRI_BGME1_CUENUM = 4;

	public const int iCRI_BGME1_BGM_SYS_E1H = 7;

	public const int iCRI_BGME1_BGM_SYS_E1 = 1;

	public const int iCRI_BGME1_BGM_ST_E1B = 5;

	public const int iCRI_BGME1_BGM_ST_E1 = 3;

	public const BGMe1 CRI_BGME1_CUENUM = BGMe1.CRI_BGME1_CUENUM;

	public const BGMe1 CRI_BGME1_BGM_SYS_E1H = BGMe1.CRI_BGME1_BGM_SYS_E1H;

	public const BGMe1 CRI_BGME1_BGM_SYS_E1 = BGMe1.CRI_BGME1_BGM_SYS_E1;

	public const BGMe1 CRI_BGME1_BGM_ST_E1B = BGMe1.CRI_BGME1_BGM_ST_E1B;

	public const BGMe1 CRI_BGME1_BGM_ST_E1 = BGMe1.CRI_BGME1_BGM_ST_E1;
}
