public enum OrangeSSType
{
	HIT = 0,
	ENEMY = 1,
	BOSS = 2,
	PLAYER = 3,
	PVE1 = 4,
	PVE2 = 5,
	PVE3 = 6,
	PVP1 = 7,
	PVP2 = 8,
	PVP3 = 9,
	PET = 10,
	VEHICLE = 11,
	EFFECT = 12,
	WEAPON = 13,
	SYSTEM = 14,
	MAPOBJS = 15,
	STAGEOBJS = 16,
	BGM = 17,
	SOUND = 18,
	VOICE = 19,
	MAX_TYPE = 20
}
