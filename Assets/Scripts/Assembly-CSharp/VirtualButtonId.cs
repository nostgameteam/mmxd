public enum VirtualButtonId
{
	SHOOT = 0,
	SKILL0 = 1,
	SKILL1 = 2,
	SELECT = 3,
	FS_SKILL = 4,
	CHIP_SWITCH = 5,
	MAX = 6
}
