using enums;

public class SocketBlackInfo
{
	public string BlackPlayerID = string.Empty;

	public UserStatus Status;

	public int Busy;

	public string FriendPlayerHUD = string.Empty;
}
