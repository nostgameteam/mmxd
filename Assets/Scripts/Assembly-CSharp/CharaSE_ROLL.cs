public enum CharaSE_ROLL
{
	NONE = 0,
	CRI_CHARASE_ROLL_CUENUM = 10,
	CRI_CHARASE_ROLL_RL_JUMP = 1,
	CRI_CHARASE_ROLL_RL_JUMP_HIGH = 2,
	CRI_CHARASE_ROLL_RL_CHAKUCHI = 3,
	CRI_CHARASE_ROLL_RL_DASH = 4,
	CRI_CHARASE_ROLL_RL_DASHEND = 5,
	CRI_CHARASE_ROLL_RL_KABEPETA = 6,
	CRI_CHARASE_ROLL_RL_KABEKERI = 7,
	CRI_CHARASE_ROLL_RL_TELEPORT_IN = 8,
	CRI_CHARASE_ROLL_RL_TELEPORT_OUT = 9,
	CRI_CHARASE_ROLL_RL_FOOT = 10
}
