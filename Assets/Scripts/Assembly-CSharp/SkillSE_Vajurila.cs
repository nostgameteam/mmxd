public enum SkillSE_Vajurila
{
	NONE = 0,
	CRI_SKILLSE_VAJURILA_CUENUM = 6,
	CRI_SKILLSE_VAJURILA_VJ_RUSH01 = 1,
	CRI_SKILLSE_VAJURILA_VJ_RUSH02_LG = 2,
	CRI_SKILLSE_VAJURILA_VJ_RUSH02_STOP = 3,
	CRI_SKILLSE_VAJURILA_VJ_HOMING01 = 7,
	CRI_SKILLSE_VAJURILA_VJ_RING01 = 5,
	CRI_SKILLSE_VAJURILA_VJ_CHARA01 = 9
}
