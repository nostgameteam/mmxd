public static class CharaSE_PALETTEi
{
	public const int iCRI_CHARASE_PALETTE_CUENUM = 10;

	public const int iCRI_CHARASE_PALETTE_PL_JUMP = 1;

	public const int iCRI_CHARASE_PALETTE_PL_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_PALETTE_PL_CHAKUCHI = 3;

	public const int iCRI_CHARASE_PALETTE_PL_DASH = 4;

	public const int iCRI_CHARASE_PALETTE_PL_DASHEND = 5;

	public const int iCRI_CHARASE_PALETTE_PL_KABEPETA = 6;

	public const int iCRI_CHARASE_PALETTE_PL_KABEKERI = 7;

	public const int iCRI_CHARASE_PALETTE_PL_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_PALETTE_PL_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_PALETTE_PL_FOOT = 10;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_CUENUM = CharaSE_PALETTE.CRI_CHARASE_PALETTE_CUENUM;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_JUMP = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_JUMP;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_JUMP_HIGH = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_JUMP_HIGH;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_CHAKUCHI = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_CHAKUCHI;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_DASH = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_DASH;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_DASHEND = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_DASHEND;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_KABEPETA = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_KABEPETA;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_KABEKERI = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_KABEKERI;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_TELEPORT_IN = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_TELEPORT_IN;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_TELEPORT_OUT = CharaSE_PALETTE.CRI_CHARASE_PALETTE_PL_TELEPORT_OUT;

	public const CharaSE_PALETTE CRI_CHARASE_PALETTE_PL_FOOT = CharaSE_PALETTE.CRI_CHARASE_PALETTE_CUENUM;
}
