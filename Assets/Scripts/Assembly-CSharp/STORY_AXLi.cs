public static class STORY_AXLi
{
	public const int iCRI_STORY_AXL_CUENUM = 16;

	public const int iCRI_STORY_AXL_A_STORY01 = 1;

	public const int iCRI_STORY_AXL_A_STORY02 = 2;

	public const int iCRI_STORY_AXL_A_STORY03 = 3;

	public const int iCRI_STORY_AXL_A_STORY04 = 4;

	public const int iCRI_STORY_AXL_A_STORY05 = 5;

	public const int iCRI_STORY_AXL_A_STORY06 = 6;

	public const int iCRI_STORY_AXL_A_STORY07 = 7;

	public const int iCRI_STORY_AXL_A_STORY08 = 8;

	public const int iCRI_STORY_AXL_A_STORY09 = 9;

	public const int iCRI_STORY_AXL_A_STORY10 = 10;

	public const int iCRI_STORY_AXL_A_STORY11 = 11;

	public const int iCRI_STORY_AXL_A_STORY12 = 12;

	public const int iCRI_STORY_AXL_A_STORY13 = 13;

	public const int iCRI_STORY_AXL_A_STORY14 = 14;

	public const int iCRI_STORY_AXL_A_STORY15 = 15;

	public const int iCRI_STORY_AXL_A_STORY16 = 16;

	public const STORY_AXL CRI_STORY_AXL_CUENUM = STORY_AXL.CRI_STORY_AXL_CUENUM;

	public const STORY_AXL CRI_STORY_AXL_A_STORY01 = STORY_AXL.CRI_STORY_AXL_A_STORY01;

	public const STORY_AXL CRI_STORY_AXL_A_STORY02 = STORY_AXL.CRI_STORY_AXL_A_STORY02;

	public const STORY_AXL CRI_STORY_AXL_A_STORY03 = STORY_AXL.CRI_STORY_AXL_A_STORY03;

	public const STORY_AXL CRI_STORY_AXL_A_STORY04 = STORY_AXL.CRI_STORY_AXL_A_STORY04;

	public const STORY_AXL CRI_STORY_AXL_A_STORY05 = STORY_AXL.CRI_STORY_AXL_A_STORY05;

	public const STORY_AXL CRI_STORY_AXL_A_STORY06 = STORY_AXL.CRI_STORY_AXL_A_STORY06;

	public const STORY_AXL CRI_STORY_AXL_A_STORY07 = STORY_AXL.CRI_STORY_AXL_A_STORY07;

	public const STORY_AXL CRI_STORY_AXL_A_STORY08 = STORY_AXL.CRI_STORY_AXL_A_STORY08;

	public const STORY_AXL CRI_STORY_AXL_A_STORY09 = STORY_AXL.CRI_STORY_AXL_A_STORY09;

	public const STORY_AXL CRI_STORY_AXL_A_STORY10 = STORY_AXL.CRI_STORY_AXL_A_STORY10;

	public const STORY_AXL CRI_STORY_AXL_A_STORY11 = STORY_AXL.CRI_STORY_AXL_A_STORY11;

	public const STORY_AXL CRI_STORY_AXL_A_STORY12 = STORY_AXL.CRI_STORY_AXL_A_STORY12;

	public const STORY_AXL CRI_STORY_AXL_A_STORY13 = STORY_AXL.CRI_STORY_AXL_A_STORY13;

	public const STORY_AXL CRI_STORY_AXL_A_STORY14 = STORY_AXL.CRI_STORY_AXL_A_STORY14;

	public const STORY_AXL CRI_STORY_AXL_A_STORY15 = STORY_AXL.CRI_STORY_AXL_A_STORY15;

	public const STORY_AXL CRI_STORY_AXL_A_STORY16 = STORY_AXL.CRI_STORY_AXL_CUENUM;
}
