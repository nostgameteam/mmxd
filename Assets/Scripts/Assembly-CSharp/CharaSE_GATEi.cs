public static class CharaSE_GATEi
{
	public const int iCRI_CHARASE_GATE_CUENUM = 10;

	public const int iCRI_CHARASE_GATE_GT_JUMP = 1;

	public const int iCRI_CHARASE_GATE_GT_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_GATE_GT_CHAKUCHI = 3;

	public const int iCRI_CHARASE_GATE_GT_DASH = 4;

	public const int iCRI_CHARASE_GATE_GT_DASHEND = 5;

	public const int iCRI_CHARASE_GATE_GT_KABEPETA = 6;

	public const int iCRI_CHARASE_GATE_GT_KABEKERI = 7;

	public const int iCRI_CHARASE_GATE_GT_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_GATE_GT_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_GATE_GT_FOOT = 10;

	public const CharaSE_GATE CRI_CHARASE_GATE_CUENUM = CharaSE_GATE.CRI_CHARASE_GATE_CUENUM;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_JUMP = CharaSE_GATE.CRI_CHARASE_GATE_GT_JUMP;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_JUMP_HIGH = CharaSE_GATE.CRI_CHARASE_GATE_GT_JUMP_HIGH;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_CHAKUCHI = CharaSE_GATE.CRI_CHARASE_GATE_GT_CHAKUCHI;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_DASH = CharaSE_GATE.CRI_CHARASE_GATE_GT_DASH;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_DASHEND = CharaSE_GATE.CRI_CHARASE_GATE_GT_DASHEND;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_KABEPETA = CharaSE_GATE.CRI_CHARASE_GATE_GT_KABEPETA;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_KABEKERI = CharaSE_GATE.CRI_CHARASE_GATE_GT_KABEKERI;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_TELEPORT_IN = CharaSE_GATE.CRI_CHARASE_GATE_GT_TELEPORT_IN;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_TELEPORT_OUT = CharaSE_GATE.CRI_CHARASE_GATE_GT_TELEPORT_OUT;

	public const CharaSE_GATE CRI_CHARASE_GATE_GT_FOOT = CharaSE_GATE.CRI_CHARASE_GATE_CUENUM;
}
