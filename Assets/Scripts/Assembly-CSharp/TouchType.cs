public enum TouchType
{
	TOUCH_BEGIN = 0,
	TOUCH_MOVE = 1,
	TOUCH_END = 2
}
