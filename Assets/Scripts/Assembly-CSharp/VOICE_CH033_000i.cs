public static class VOICE_CH033_000i
{
	public const int iCRI_VOICE_CH033_000_CUENUM = 29;

	public const int iCRI_VOICE_CH033_000_V_CH033_HURT01 = 1;

	public const int iCRI_VOICE_CH033_000_V_CH033_ATTACK01 = 25;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL01 = 8;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL02 = 9;

	public const int iCRI_VOICE_CH033_000_V_CH033_PINCH01 = 20;

	public const int iCRI_VOICE_CH033_000_V_CH033_PINCH02 = 21;

	public const int iCRI_VOICE_CH033_000_V_CH033_PINCH03 = 22;

	public const int iCRI_VOICE_CH033_000_V_CH033_LOSE01 = 5;

	public const int iCRI_VOICE_CH033_000_V_CH033_UNIQUE01 = 6;

	public const int iCRI_VOICE_CH033_000_V_CH033_WIN01 = 3;

	public const int iCRI_VOICE_CH033_000_V_CH033_JUMP01 = 12;

	public const int iCRI_VOICE_CH033_000_V_CH033_SABER01 = 14;

	public const int iCRI_VOICE_CH033_000_V_CH033_SABER03 = 16;

	public const int iCRI_VOICE_CH033_000_V_CH033_SABER05 = 18;

	public const int iCRI_VOICE_CH033_000_V_CH033_START01 = 2;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL01_1 = 10;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL01_2 = 31;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL01_3 = 32;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL02_1 = 11;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL02_2 = 33;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL02_3 = 34;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA01 = 26;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA02 = 27;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA03 = 28;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA04 = 29;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA05 = 30;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL03 = 35;

	public const int iCRI_VOICE_CH033_000_V_CH033_SKILL04 = 36;

	public const int iCRI_VOICE_CH033_000_V_CH033_CHARA06 = 37;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_CUENUM = VOICE_CH033_000.CRI_VOICE_CH033_000_CUENUM;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_HURT01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_HURT01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_ATTACK01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_ATTACK01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL02 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL02;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_PINCH01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_PINCH01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_PINCH02 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_PINCH02;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_PINCH03 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_PINCH03;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_LOSE01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_LOSE01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_UNIQUE01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_UNIQUE01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_WIN01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_WIN01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_JUMP01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_JUMP01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SABER01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SABER01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SABER03 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SABER03;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SABER05 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SABER05;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_START01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_START01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL01_1 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL01_1;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL01_2 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL01_2;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL01_3 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL01_3;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL02_1 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL02_1;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL02_2 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL02_2;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL02_3 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL02_3;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA01 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_CHARA01;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA02 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_CHARA02;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA03 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_CHARA03;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA04 = VOICE_CH033_000.CRI_VOICE_CH033_000_CUENUM;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA05 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_CHARA05;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL03 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL03;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_SKILL04 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_SKILL04;

	public const VOICE_CH033_000 CRI_VOICE_CH033_000_V_CH033_CHARA06 = VOICE_CH033_000.CRI_VOICE_CH033_000_V_CH033_CHARA06;
}
