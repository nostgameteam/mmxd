public class FxNames
{
	public const string Bullet_GoldFire_Small_Impact = "Bullet_GoldFire_Small_Impact";

	public const string Bullet_GoldFire_Small_MuzzleFlare = "Bullet_GoldFire_Small_MuzzleFlare";

	public const string Bullet_GoldFire_Small_Projectile = "Bullet_GoldFire_Small_Projectile";

	public const string PlasmaOceanBlue_Small_Impact = "PlasmaOceanBlue_Small_Impact";

	public const string PlasmaOceanBlue_Small_MuzzleFlare = "PlasmaOceanBlue_Small_MuzzleFlare";

	public const string PlasmaOceanBlue_Small_Projectile = "PlasmaOceanBlue_Small_Projectile";

	public const string PlasmaYellow_Small_Impact = "PlasmaYellow_Small_Impact";

	public const string PlasmaYellow_Small_MuzzleFlare = "PlasmaYellow_Small_MuzzleFlare";

	public const string PlasmaYellow_Small_Projectile = "PlasmaYellow_Small_Projectile";

	public const string ChargeIdle = "ChargeIdle";

	public const string ChargeLV1 = "ChargeLV1";

	public const string ChargeLV2 = "ChargeLV2";

	public const string ChargeSteps = "ChargeSteps";

	public const string Sphere = "Sphere";

	public const string DashThruster = "DashThruster";

	public const string DustFire = "DustFire";

	public const string Explosion = "Explosion";

	public const string ExplosionRoundFireX = "ExplosionRoundFireX";

	public const string FireThrower = "FireThrower";

	public const string FireThrower1 = "FireThrower1";

	public const string FireThrower2 = "FireThrower2";

	public const string FireThrowerBullet = "FireThrowerBullet";

	public const string JumpSlam = "JumpSlam";

	public const string LandFx = "LandFx";

	public const string KickJump = "KickJump";

	public const string KickJump1 = "KickJump1";

	public const string WallKickSpark = "WallKickSpark";

	public const string WallKickSparkold = "WallKickSparkold";

	public const string LightnBullet = "LightnBullet";

	public const string LightnBulletS = "LightnBulletS";

	public const string lightning_gun_beam = "lightning_gun_beam";

	public const string MissileFlame = "MissileFlame";

	public const string MissileSmokeTrail = "MissileSmokeTrail";

	public const string rockmanstart = "rockmanstart";

	public const string rockmanstart2 = "rockmanstart2";

	public const string rockmanstart3 = "rockmanstart3";

	public const string RockmanDust = "RockmanDust";

	public const string ShotgunBullet = "ShotgunBullet";

	public const string FX_MOB_EXPLODE0 = "FX_MOB_EXPLODE0";

	public const string FX_MOB_EXPLODE1 = "FX_MOB_EXPLODE1";

	public const string FX_MOB_EXPLODE2 = "fxuse_mob_explode_000";

	public const string fxhit_slash_000 = "fxhit_slash_000";

	public const string p_circleammo_000 = "p_circleammo_000";

	public const string p_groundammo_000 = "p_groundammo_000";

	public const string FX_Point_Light_01 = "FX_Point_Light_01";

	public const string fxhit_explode_001 = "fxhit_explode_001";

	public const string p_melt_000 = "p_melt_000";

	public const string p_missile_003 = "p_missile_003";

	public const string FX_DASH_SMOKE = "FX_DASH_SMOKE";

	public const string FX_DASH_SPEEDLINE = "FX_DASH_SPEEDLINE";

	public const string OBJ_DASH_SMOKE = "OBJ_DASH_SMOKE";

	public const string FX_DASH_SPARK = "FX_DASH_SPARK";

	public const string OBJ_DASH_SPARK = "OBJ_DASH_SPARK";

	public const string FX_JUMP_LEFT = "FX_JUMP_LEFT";

	public const string OBJ_JUMP_LEFT = "OBJ_JUMP_LEFT";

	public const string FX_JUMP_RIGHT = "FX_JUMP_RIGHT";

	public const string OBJ_JUMP_RIGHT = "OBJ_JUMP_RIGHT";

	public const string FX_JUMP_UP = "FX_JUMP_UP";

	public const string OBJ_JUMP_UP = "OBJ_JUMP_UP";

	public const string FX_LAND = "FX_LAND";

	public const string OBJ_LAND = "OBJ_LAND";

	public const string FX_PLAYER_DIE = "FX_PLAYER_DIE";

	public const string FX_TELEPORT_IN = "FX_TELEPORT_IN";

	public const string FX_TELEPORT_IN_OLD = "FX_TELEPORT_IN_OLD";

	public const string FX_TELEPORT_IN2 = "FX_TELEPORT_IN2";

	public const string FX_TELEPORT_OUT = "FX_TELEPORT_OUT";

	public const string FX_WALLKICK_SPARK = "FX_WALLKICK_SPARK";

	public const string OBJ_WALLKICK_SPARK = "OBJ_WALLKICK_SPARK";

	public const string fxduring_chargeshot_000 = "fxduring_chargeshot_000";

	public const string fxduring_chargeshot_000_end = "fxduring_chargeshot_000_end";

	public const string fxduring_chargeshot_000_start = "fxduring_chargeshot_000_start";

	public const string fxduring_chargeshot_001 = "fxduring_chargeshot_001";

	public const string fxduring_chargeshot_001_end = "fxduring_chargeshot_001_end";

	public const string fxduring_chargeshot_001_loop = "fxduring_chargeshot_001_loop";

	public const string fxduring_chargeshot_001_start = "fxduring_chargeshot_001_start";

	public const string fxuse_chargeshot_000 = "fxuse_chargeshot_000";

	public const string fxuse_chargeshot_000_f = "fxuse_chargeshot_000_f";

	public const string fxuse_chargeshot_000_new = "fxuse_chargeshot_000_new";

	public const string p_chargeshot_000 = "p_chargeshot_000";

	public const string p_chargeshot_001 = "p_chargeshot_001";

	public const string p_chargeshot_002 = "p_chargeshot_002";

	public const string p_missile_002 = "p_missile_002";

	public const string fxuse_flameblade_000 = "fxuse_flameblade_000";

	public const string fxuse_flameblade_000_a = "fxuse_flameblade_000_a";

	public const string fxuse_flameblade_000_b = "fxuse_flameblade_000_b";

	public const string fxuse_flameblade_001 = "fxuse_flameblade_001";

	public const string fxuse_flameblade_001_a = "fxuse_flameblade_001_a";

	public const string fxuse_flameblade_001_b = "fxuse_flameblade_001_b";

	public const string p_fallen_000 = "p_fallen_000";

	public const string fxduring_em059_icerevert_000 = "fxduring_em059_icerevert_000";

	public const string fxhit_em059_icebroken_000 = "fxhit_em059_icebroken_000";

	public const string fxuse_em059_icerevert_000 = "fxuse_em059_icerevert_000";

	public const string p_em059_icefragment_000 = "p_em059_icefragment_000";

	public const string fxduring_em060_rockrevert_000 = "fxduring_em060_rockrevert_000";

	public const string fxhit_em060_rockbroken_000 = "fxhit_em060_rockbroken_000";

	public const string fxuse_em060_rockrevert_000 = "fxuse_em060_rockrevert_000";

	public const string p_em060_rockfragment_000 = "p_em060_rockfragment_000";

	public const string fxuse_e_at_000 = "fxuse_e_at_000";

	public const string p_e_at_000 = "p_e_at_000";
}
