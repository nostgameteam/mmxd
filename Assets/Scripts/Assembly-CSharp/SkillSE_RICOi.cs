public static class SkillSE_RICOi
{
	public const int iCRI_SKILLSE_RICO_CUENUM = 26;

	public const int iCRI_SKILLSE_RICO_RI_CHARA01 = 40;

	public const int iCRI_SKILLSE_RICO_RI_CHARA02 = 41;

	public const int iCRI_SKILLSE_RICO_RI_CHARA03 = 42;

	public const int iCRI_SKILLSE_RICO_RI_REPLO02_LP = 2;

	public const int iCRI_SKILLSE_RICO_RI_SOCK01_STOP = 14;

	public const int iCRI_SKILLSE_RICO_RI_SOCK01_LP = 13;

	public const int iCRI_SKILLSE_RICO_RI_SOCK01 = 12;

	public const int iCRI_SKILLSE_RICO_RI_SOCK02 = 15;

	public const int iCRI_SKILLSE_RICO_RI_REPLO02_STOP = 3;

	public const int iCRI_SKILLSE_RICO_RI_REPLO01 = 1;

	public const int iCRI_SKILLSE_RICO_RI_ANALYZE01 = 5;

	public const int iCRI_SKILLSE_RICO_RI_FORALL01 = 23;

	public const int iCRI_SKILLSE_RICO_RI_ANALYZE02 = 6;

	public const int iCRI_SKILLSE_RICO_RI_REPLO03 = 7;

	public const int iCRI_SKILLSE_RICO_RI_REPLO04_2_LP = 9;

	public const int iCRI_SKILLSE_RICO_RI_REPLO04_2_STOP = 10;

	public const int iCRI_SKILLSE_RICO_RI_REPLO04_1 = 8;

	public const int iCRI_SKILLSE_RICO_RI_REPLO05 = 11;

	public const int iCRI_SKILLSE_RICO_RI_STAR01 = 17;

	public const int iCRI_SKILLSE_RICO_RI_STAR02_LP = 18;

	public const int iCRI_SKILLSE_RICO_RI_STAR02_STOP = 19;

	public const int iCRI_SKILLSE_RICO_RI_START02 = 80;

	public const int iCRI_SKILLSE_RICO_RI_START03 = 81;

	public const int iCRI_SKILLSE_RICO_RI_FORALL02 = 24;

	public const int iCRI_SKILLSE_RICO_RI_RIBBON01 = 20;

	public const int iCRI_SKILLSE_RICO_RI_RIBBON02 = 21;

	public const SkillSE_RICO CRI_SKILLSE_RICO_CUENUM = SkillSE_RICO.CRI_SKILLSE_RICO_CUENUM;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_CHARA01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_CHARA01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_CHARA02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_CHARA02;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_CHARA03 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_CHARA03;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO02_LP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO02_LP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_SOCK01_STOP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_SOCK01_STOP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_SOCK01_LP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_SOCK01_LP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_SOCK01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_SOCK01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_SOCK02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_SOCK02;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO02_STOP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO02_STOP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_ANALYZE01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_ANALYZE01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_FORALL01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_FORALL01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_ANALYZE02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_ANALYZE02;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO03 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO03;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO04_2_LP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO04_2_LP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO04_2_STOP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO04_2_STOP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO04_1 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO04_1;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_REPLO05 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_REPLO05;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_STAR01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_STAR01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_STAR02_LP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_STAR02_LP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_STAR02_STOP = SkillSE_RICO.CRI_SKILLSE_RICO_RI_STAR02_STOP;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_START02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_START02;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_START03 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_START03;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_FORALL02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_FORALL02;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_RIBBON01 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_RIBBON01;

	public const SkillSE_RICO CRI_SKILLSE_RICO_RI_RIBBON02 = SkillSE_RICO.CRI_SKILLSE_RICO_RI_RIBBON02;
}
