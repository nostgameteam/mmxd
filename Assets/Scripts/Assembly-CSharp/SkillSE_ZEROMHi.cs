public static class SkillSE_ZEROMHi
{
	public const int iCRI_SKILLSE_ZEROMH_CUENUM = 10;

	public const int iCRI_SKILLSE_ZEROMH_ZM_KAKUYOU = 1;

	public const int iCRI_SKILLSE_ZEROMH_ZM_RYUKI02 = 10;

	public const int iCRI_SKILLSE_ZEROMH_ZM_DASH = 5;

	public const int iCRI_SKILLSE_ZEROMH_ZM_KYOUSEI = 6;

	public const int iCRI_SKILLSE_ZEROMH_ZM_CHARA01 = 40;

	public const int iCRI_SKILLSE_ZEROMH_ZM_START01 = 80;

	public const int iCRI_SKILLSE_ZEROMH_ZM_SUISEI = 2;

	public const int iCRI_SKILLSE_ZEROMH_ZM_RYUKI01_LP = 8;

	public const int iCRI_SKILLSE_ZEROMH_ZM_RYUKI01_STOP = 9;

	public const int iCRI_SKILLSE_ZEROMH_ZM_BOUSOU = 3;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_CUENUM = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_CUENUM;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_KAKUYOU = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_KAKUYOU;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_RYUKI02 = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_CUENUM;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_DASH = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_DASH;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_KYOUSEI = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_KYOUSEI;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_CHARA01 = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_CHARA01;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_START01 = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_START01;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_SUISEI = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_SUISEI;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_RYUKI01_LP = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_RYUKI01_LP;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_RYUKI01_STOP = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_RYUKI01_STOP;

	public const SkillSE_ZEROMH CRI_SKILLSE_ZEROMH_ZM_BOUSOU = SkillSE_ZEROMH.CRI_SKILLSE_ZEROMH_ZM_BOUSOU;
}
