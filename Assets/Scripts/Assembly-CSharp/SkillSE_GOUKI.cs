public enum SkillSE_GOUKI
{
	NONE = 0,
	CRI_SKILLSE_GOUKI_CUENUM = 6,
	CRI_SKILLSE_GOUKI_GO_CHARA01 = 40,
	CRI_SKILLSE_GOUKI_GO_START01 = 80,
	CRI_SKILLSE_GOUKI_GO_SPINING = 1,
	CRI_SKILLSE_GOUKI_GO_SHUNGOKUSATSU03 = 5,
	CRI_SKILLSE_GOUKI_GO_SHUNGOKUSATSU01 = 3,
	CRI_SKILLSE_GOUKI_GO_SHUNGOKUSATSU02 = 4
}
