namespace Line.LineSDK
{
	public class LoginOption
	{
		public bool OnlyWebLogin { get; set; }

		public string BotPrompt { get; set; }
	}
}
