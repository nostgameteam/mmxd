public static class BGMe3i
{
	public const int iCRI_BGME3_CUENUM = 2;

	public const int iCRI_BGME3_BGM_ST_E3 = 3;

	public const int iCRI_BGME3_BGM_SYS_E3 = 1;

	public const BGMe3 CRI_BGME3_CUENUM = BGMe3.CRI_BGME3_CUENUM;

	public const BGMe3 CRI_BGME3_BGM_ST_E3 = BGMe3.CRI_BGME3_BGM_ST_E3;

	public const BGMe3 CRI_BGME3_BGM_SYS_E3 = BGMe3.CRI_BGME3_BGM_SYS_E3;
}
