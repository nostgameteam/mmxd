public enum CueType
{
	CT_NORMAL = 0,
	CT_LOOP = 1,
	CT_STOP = 2
}
