public static class VOICE_AwakenZeroi
{
	public const int iCRI_VOICE_AWAKENZERO_CUENUM = 21;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_HURT01 = 1;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_ATTACK01 = 25;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SKILL01 = 8;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SKILL02 = 9;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_PINCH01 = 20;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_PINCH02 = 21;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_PINCH03 = 22;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_LOSE01 = 5;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_UNIQUE01 = 6;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_UNIQUE02 = 7;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_WIN01 = 3;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_JUMP01 = 12;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SABER01 = 14;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SABER03 = 16;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SABER05 = 18;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_START01 = 2;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_CHARA01 = 26;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_CHARA04 = 40;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_CHARA02 = 27;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_CHARA03 = 28;

	public const int iCRI_VOICE_AWAKENZERO_V_ZA_SKILL03 = 10;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_CUENUM = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_CUENUM;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_HURT01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_HURT01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_ATTACK01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_ATTACK01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SKILL01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SKILL01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SKILL02 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SKILL02;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_PINCH01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_PINCH01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_PINCH02 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_CUENUM;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_PINCH03 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_PINCH03;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_LOSE01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_LOSE01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_UNIQUE01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_UNIQUE01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_UNIQUE02 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_UNIQUE02;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_WIN01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_WIN01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_JUMP01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_JUMP01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SABER01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SABER01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SABER03 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SABER03;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SABER05 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SABER05;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_START01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_START01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_CHARA01 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_CHARA01;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_CHARA04 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_CHARA04;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_CHARA02 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_CHARA02;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_CHARA03 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_CHARA03;

	public const VOICE_AwakenZero CRI_VOICE_AWAKENZERO_V_ZA_SKILL03 = VOICE_AwakenZero.CRI_VOICE_AWAKENZERO_V_ZA_SKILL03;
}
