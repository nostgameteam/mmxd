public class OrangeCrcInfo
{
	public string Name;

	public string Crc;

	public long Size;

	public OrangeCrcInfo(string p_name, string p_crc, long p_size)
	{
		Name = p_name;
		Crc = p_crc;
		Size = p_size;
	}
}
