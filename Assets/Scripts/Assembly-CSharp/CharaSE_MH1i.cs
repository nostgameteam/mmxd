public static class CharaSE_MH1i
{
	public const int iCRI_CHARASE_MH1_CUENUM = 10;

	public const int iCRI_CHARASE_MH1_MH1_JUMP = 1;

	public const int iCRI_CHARASE_MH1_MH1_JUMP_HIGH = 2;

	public const int iCRI_CHARASE_MH1_MH1_CHAKUCHI = 3;

	public const int iCRI_CHARASE_MH1_MH1_DASH = 4;

	public const int iCRI_CHARASE_MH1_MH1_DASHEND = 5;

	public const int iCRI_CHARASE_MH1_MH1_KABEPETA = 6;

	public const int iCRI_CHARASE_MH1_MH1_KABEKERI = 7;

	public const int iCRI_CHARASE_MH1_MH1_TELEPORT_IN = 8;

	public const int iCRI_CHARASE_MH1_MH1_TELEPORT_OUT = 9;

	public const int iCRI_CHARASE_MH1_MH1_FOOT = 10;

	public const CharaSE_MH1 CRI_CHARASE_MH1_CUENUM = CharaSE_MH1.CRI_CHARASE_MH1_CUENUM;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_JUMP = CharaSE_MH1.CRI_CHARASE_MH1_MH1_JUMP;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_JUMP_HIGH = CharaSE_MH1.CRI_CHARASE_MH1_MH1_JUMP_HIGH;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_CHAKUCHI = CharaSE_MH1.CRI_CHARASE_MH1_MH1_CHAKUCHI;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_DASH = CharaSE_MH1.CRI_CHARASE_MH1_MH1_DASH;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_DASHEND = CharaSE_MH1.CRI_CHARASE_MH1_MH1_DASHEND;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_KABEPETA = CharaSE_MH1.CRI_CHARASE_MH1_MH1_KABEPETA;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_KABEKERI = CharaSE_MH1.CRI_CHARASE_MH1_MH1_KABEKERI;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_TELEPORT_IN = CharaSE_MH1.CRI_CHARASE_MH1_MH1_TELEPORT_IN;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_TELEPORT_OUT = CharaSE_MH1.CRI_CHARASE_MH1_MH1_TELEPORT_OUT;

	public const CharaSE_MH1 CRI_CHARASE_MH1_MH1_FOOT = CharaSE_MH1.CRI_CHARASE_MH1_CUENUM;
}
