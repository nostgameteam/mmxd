public static class SkillSE_XGaiai
{
	public const int iCRI_SKILLSE_XGAIA_CUENUM = 8;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGESHOT_S = 3;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGESHOT_M = 4;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGESHOT_L = 5;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGE_LP = 1;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGE_STOP = 2;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARGEMAX = 6;

	public const int iCRI_SKILLSE_XGAIA_XG_GIGA01 = 8;

	public const int iCRI_SKILLSE_XGAIA_XG_CHARA01 = 40;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_CUENUM = SkillSE_XGaia.CRI_SKILLSE_XGAIA_CUENUM;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGESHOT_S = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGESHOT_S;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGESHOT_M = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGESHOT_M;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGESHOT_L = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGESHOT_L;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGE_LP = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGE_LP;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGE_STOP = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGE_STOP;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARGEMAX = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARGEMAX;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_GIGA01 = SkillSE_XGaia.CRI_SKILLSE_XGAIA_CUENUM;

	public const SkillSE_XGaia CRI_SKILLSE_XGAIA_XG_CHARA01 = SkillSE_XGaia.CRI_SKILLSE_XGAIA_XG_CHARA01;
}
