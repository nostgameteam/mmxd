public enum SkillSE_X4thArmor
{
	NONE = 0,
	CRI_SKILLSE_X4THARMOR_CUENUM = 8,
	CRI_SKILLSE_X4THARMOR_X4_CHARGESHOT_S = 3,
	CRI_SKILLSE_X4THARMOR_X4_CHARGESHOT_M = 4,
	CRI_SKILLSE_X4THARMOR_X4_CHARGESHOT_L = 5,
	CRI_SKILLSE_X4THARMOR_X4_CHARGE_LP = 1,
	CRI_SKILLSE_X4THARMOR_X4_CHARGE_STOP = 2,
	CRI_SKILLSE_X4THARMOR_X4_NOVASTRIKE = 7,
	CRI_SKILLSE_X4THARMOR_X4_START01 = 80,
	CRI_SKILLSE_X4THARMOR_X4_CHARA01 = 40
}
