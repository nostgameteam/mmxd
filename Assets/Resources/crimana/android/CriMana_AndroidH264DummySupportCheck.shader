Shader "CriMana/AndroidH264DummySupportCheck" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader {
		Pass {
			ZTest Always
			ZWrite Off
			Cull Off
			GpuProgramID 35932
			// No subprograms found
		}
	}
}